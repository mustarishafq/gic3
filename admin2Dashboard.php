<?php

require_once dirname(__FILE__) . '/1dbCon/dbCon.php';
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/adminAccess2.php';
require_once dirname(__FILE__) . '/classes/Invoice.php';
require_once dirname(__FILE__) . '/classes/Product2.php';
require_once dirname(__FILE__) . '/classes/LoanStatus.php';

require_once dirname(__FILE__) . '/utilities/allNoticeModals.php';
require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';
require_once dirname(__FILE__) . '/utilities/languageFunction.php';

$conn = connDB();
$caseStatus = 'COMPLETED';
$loanDetails = getLoanStatus($conn, "WHERE case_status = ?",array("case_status"),array($caseStatus),"s");
?>

<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title>Admin 2 Dashboard</title>
  </head>
  <body>
<h1>Admin Dashboard</h1>

<div class="tempo-two-input-clear"></div>

<div class="dual-input-div">
  <a href="#"><p>New Project</p></a> 
</div>
<div class="dual-input-div second-dual-input">
  <a href="editInvoiceGeneral.php"><p>Issue Invoice</p></a> 
</div>
<div class="dual-input-div">
  <a href="#"><p>Issue Payroll</p></a> 
</div>

<div class="tempo-two-input-clear"></div>

<div class="">
  <button type="button" name="button" onclick="location.href='adminAdvanced.php'">
    Advanced (<?php
      echo count($loanDetails);
     ?>)
  </button>
</div>

  </body>
</html>
