<div class="headerline"></div>
<div class="width100 same-padding header-div">
	<a href="admin1Product.php"><div class="logo"><img src="img/logo.png" class="logo-size"></div></a>
	<div class="right-menu">
    	<a href="adminAddNewProject.php" class="menu-right-margin hover1">
        	<img src="img/project.png" class="hover1a gic-menu-icon" alt="Add New Project" title="Add New Project">
            <img src="img/project2.png" class="hover1b gic-menu-icon" alt="Add New Project" title="Add New Project">
        </a>
    	<div class="dropdown hover1">
        	<a  class="menu-right-margin dropdown-btn">
            	<img src="img/invoice.png" class="gic-menu-icon hover1a" alt="Loan Status" title="Loan Status">
            	<img src="img/invoice2.png" class="gic-menu-icon hover1b" alt="Loan Status" title="Loan Status">
            </a>
            <div class="dropdown-content yellow-dropdown-content">
				<p class="dropdown-p"><a href="admin1Product.php"  class="dropdown-a">Loan Status Form</a></p>
				<p class="dropdown-p"><a href="adminAddNewProduct.php"  class="dropdown-a">Add New Loan</a></p>
				<p class="dropdown-p"><a href="uploadBookingForm.php"  class="dropdown-a">Upload Loan Form</a></p>
            </div>
    	</div>
    	<a href="logout.php" class="hover1">
        	<img src="img/logout.png" class="hover1a gic-menu-icon" alt="logout" title="logout">
            <img src="img/logout2.png" class="hover1b gic-menu-icon" alt="logout" title="logout">
        </a>
    </div>
</div>
<div class="clear"></div>
