<?php
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';
require_once dirname(__FILE__) . '/adminAccess1.php';
require_once dirname(__FILE__) . '/sessionLoginChecker.php';

require_once dirname(__FILE__) . '/classes/LoanStatus.php';
require_once dirname(__FILE__) . '/classes/PaymentMethod.php';
require_once dirname(__FILE__) . '/classes/BankName.php';
require_once dirname(__FILE__) . '/classes/Project.php';
require_once dirname(__FILE__) . '/classes/User.php';

require_once dirname(__FILE__) . '/utilities/allNoticeModals.php';
require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';
require_once dirname(__FILE__) . '/utilities/languageFunction.php';

$conn = connDB();

$agentList = getUser($conn, "WHERE user_type = 3");
$paymentList = getPaymentList($conn);
$bankList = getBankName($conn);
$projectList = getProject($conn);

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <?php require_once dirname(__FILE__) . '/header.php'; ?>
	<?php include 'meta.php'; ?>
    <!--<meta property="og:url" content="https://dcksupreme.asia/" />-->
    <meta property="og:title" content="Add | GIC" />
    <title>Add | GIC</title>
    <!--<link rel="canonical" href="https://dcksupreme.asia/" />-->
    <?php include 'css.php'; ?>
</head>
<body class="body">

<?php  include 'admin1Header.php'; ?>

<?php echo '<script type="text/javascript" src="js/jquery-3.3.1.min.js"></script>'; ?>
<div class="yellow-body padding-from-menu same-padding">
<h1 class="username">Add New Booking</h1>

  <form  action="utilities/addNewProductFunction.php" method="POST" enctype="multipart/form-data">

    <div class="dual-input-div">
    <p>Project Name</p>
      <select class="dual-input clean" name="project_name">
        <option value="">Please Select a Project</option>
        <?php for ($cntPro=0; $cntPro <count($projectList) ; $cntPro++)
        {
        ?>
          <option value="<?php echo $projectList[$cntPro]->getProjectName(); ?>">
            <?php echo $projectList[$cntPro]->getProjectName(); ?>
          </option>
      <?php
      }
      ?>
      </select>
    </div>
    <div class="dual-input-div second-dual-input">
      <p>Unit No.</p>
      <input class="dual-input clean" type="text" placeholder="Unt No." id="unit_no" name="unit_no" required>
    </div>

    <div class="tempo-two-input-clear"></div>

      <div class="dual-input-div">
          <p>Purchaser Name<img  id="remBtn" width="13px" align="right" src="img/mmm.png"><img id="addBtn" width="13px" align="right" src="img/ppp.png"></p>
          <input class="dual-input clean" type="text" placeholder="Purchaser Name" id="purchaser_nameOri" name="purchaser_name[]" required>

        </div>
        <div class="dual-input-div second-dual-input">
          <p>Contact</p>
          <input class="dual-input clean" type="text" placeholder="Contact" id="contact" name="contact[]" required>
        </div>

        <div class="tempo-two-input-clear"></div>
        <div class="dual-input-div">
          <p>IC</p>
          <!-- <input oninput="this.value = this.value.toUpperCase()" class="dual-input clean" type="number" placeholder="IC" id="ic" name="ic" > -->
          <input class="dual-input clean" type="text" placeholder="IC" id="ic" name="ic[]" required>
        </div>
        <div class="dual-input-div second-dual-input">
          <p>E-mail</p>
          <input class="dual-input clean" type="text" placeholder="E-mail" id="email" name="email[]">
        </div>
          <p id="addIn"></p>
    <div class="tempo-two-input-clear"></div>
    <div class="dual-input-div">
      <p>Booking Date</p>
      <input class="dual-input clean" type="date" id="booking_date" name="booking_date" value="<?php echo date('Y-m-d') ?>" required>
    </div>

    <div class="tempo-two-input-clear"></div>
    <div class="dual-input-div">
      <p>Sq Ft</p>
      <input class="dual-input clean" type="text" placeholder="Square Feet" id="sq_ft" name="sq_ft">
    </div>
    <div class="dual-input-div second-dual-input">
      <p>SPA Price</p>
      <input  class="dual-input clean" type="text" placeholder="SPA Price" id="spa_price" name="spa_price" required>
    </div>

    <div class="tempo-two-input-clear"></div>
    <div class="dual-input-div">
      <p>Package</p>
      <input class="dual-input clean" type="text" placeholder="Package" id="package" name="package">
    </div>
    <div class="dual-input-div second-dual-input">
      <p>Discount</p>
      <input class="dual-input clean" type="text" placeholder="Discount" id="discount" name="discount">
    </div>

    <div class="tempo-two-input-clear"></div>
    <div class="dual-input-div">
      <p>Rebate</p>
      <input class="dual-input clean" type="text" placeholder="Rebate" id="rebate" name="rebate">
    </div>
    <div class="dual-input-div second-dual-input">
      <p>Extra Rebate</p>
      <input class="dual-input clean" type="text" placeholder="Extra Rebate" id="extra_rebate" name="extra_rebate">
    </div>

    <!-- <div class="tempo-two-input-clear"></div>
    <div class="dual-input-div">
      <p>Nett Price (RM)</p>
      <input class="dual-input clean" type="text" placeholder="Nett Price" id="nettprice" name="nettprice">
    </div>
    <div class="dual-input-div second-dual-input">
      <p>Total Developer Commission</p>
      <input  class="dual-input clean" type="text" placeholder="Total Developer Commission" id="totaldevelopercomm" name="totaldevelopercomm" >
    </div> -->

    <div class="three-input-div dual-input-div">
      <p>Nett Price (RM)</p>
      <input class="dual-input clean" type="text" placeholder="Nett Price" id="nettprice" name="nettprice">
    </div>
    <div class="three-input-div second-three-input dual-input-div">
      <p>Total Developer Commission</p>
      <input  class="dual-input clean" type="text" placeholder="Total Developer Commission" id="totaldevelopercomm" name="totaldevelopercomm">
    </div>
    <div class="three-input-div dual-input-div">
      <p>Total Developer Commission (%)</p>
      <input  class="dual-input clean" type="text" placeholder="Total Developer Commission (%)" id="totaldevelopercommper" name="totaldevelopercommper" >
    </div>

    <div class="tempo-two-input-clear"></div>
    <div class="dual-input-div">
    <p>Agent</p>
    <!-- <input oninput="this.value = this.value.toUpperCase()" class="dual-input clean" type="text" id="agent" name="agent" > -->
    <select class="dual-input clean" name="agent">
      <option value="">Please Select a Agent</option>
      <?php for ($cnt=0; $cnt <count($agentList) ; $cnt++)
      {
      ?>
        <option value="<?php echo $agentList[$cnt]->getUsername(); ?>">
          <?php echo $agentList[$cnt]->getUsername(); ?>
        </option>
      <?php
      }
      ?>
    </select>
    </div>
    <div class="dual-input-div second-dual-input">
      <p>Loan Status</p>
      <input  class="dual-input clean" type="text" placeholder="Loan Status" id="loanstatus" name="loanstatus" >
      <!-- <select class="dual-input clean" name="loanstatus">
        <option value="">Please Select a Status</option>
        <option value="PENDING">Pending</option>
        <option value="COMPLETED">Completed</option>
      </select> -->
    </div>

    <div class="tempo-two-input-clear"></div>
    <div class="dual-input-div">
    <p>Remark</p>
    <input class="dual-input clean" type="text" id="remark" name="remark" >
    </div>
    <div class="dual-input-div second-dual-input">
      <p>B Form Collected</p>
      <select class="dual-input clean" name="bform_Collected">
        <option value="">Please Select an Option</option>
        <option value="YES">Yes</option>
        <option value="NO">No</option>
    </select>
    </div>

    <div class="tempo-two-input-clear"></div>
    <div class="dual-input-div">
      <p>Payment Method</p>
      <!-- <input class="dual-input clean" type="text" placeholder="" id="payment_method" name="payment_method" > -->
      <!-- <select class="dual-input clean" name="payment_method" >
        <option value="">Please Select a Payment Method</option>
        <option value="GIC MERCHANT">GIC MERCHANT</option>
        <option value="TT TO DEVELOPER">TT TO DEVELOPER</option>
        <option value="DEVELOPER MERCHANT">DEVELOPER MERCHANT</option>
        <option value="TT TO GIC">TT TO GIC</option>
      </select> -->

<select class="dual-input clean" name="payment_method">
  <option value="">Select an option</option>
  <?php for ($cnt=0; $cnt <count($paymentList) ; $cnt++)
  {
  ?>
    <option value="<?php echo $paymentList[$cnt]->getPaymentMethod() ?>">
      <?php echo $paymentList[$cnt]->getPaymentMethod(); ?>
    </option>
  <?php
  }
  ?>
</select>

    </div>
    <div class="dual-input-div second-dual-input">
      <p>Lawyer</p>
      <input class="dual-input clean" type="text" placeholder="" id="lawyer" name="lawyer" >
    </div>

    <div class="tempo-two-input-clear"></div>
    <div class="dual-input-div">
    <p>Pending Approval Status</p>
      <select class="dual-input clean" name="pending_approval_status" >
        <option value="">Please Select an Option</option>
        <option value="YES">Yes</option>
        <option value="NO">No</option>
      </select>
    </div>
    <div class="dual-input-div second-dual-input">
      <p>Bank Approved</p>
      <!-- <input class="dual-input clean" type="text" id="bank_approved" name="bank_approved"> -->
      <select class="dual-input clean" name="bank_approved" >
        <option value="">Please Select a Bank</option>
        <?php if ($bankList) {
          for ($cnt=0; $cnt <count($bankList) ; $cnt++) {
          ?><option value="<?php echo $bankList[$cnt]->getBankName() ?>"><?php echo $bankList[$cnt]->getBankName() ?></option> <?php
          }
        } ?>
        <!-- <option value="ABMB">ABMB</option>
        <option value="AMMB">AMMB</option>
        <option value="BIMB">BIMB</option>
        <option value="BKRM">BKRM</option>
        <option value="CIMB">CIMB</option>
        <option value="CITI">CITI</option>
        <option value="DBB">DBB</option>
        <option value="HLBB">HLBB</option>
        <option value="HSBC">HSBC</option>
        <option value="MBB">MBB</option>
        <option value="OCBC">OCBC</option>
        <option value="PBB">PBB</option>
        <option value="RHB">RHB</option>
        <option value="SCB">SCB</option>
        <option value="UOB">UOB</option> -->
      </select>
    </div>

    <div class="tempo-two-input-clear"></div>
    <div class="dual-input-div">
      <p>Lo Signed Date</p>
      <input class="dual-input clean" type="date" placeholder="" value="<?php echo date('Y-m-d') ?>" id="lo_signed_date" name="lo_signed_date">
    </div>
    <div class="dual-input-div second-dual-input">
      <p>La Signed Date</p>
      <input class="dual-input clean" type="date" value="<?php echo date('Y-m-d') ?>" placeholder="" id="la_signed_date" name="la_signed_date" >
    </div>

    <div class="tempo-two-input-clear"></div>
    <div class="dual-input-div">
      <p>Spa Signed Date</p>
      <input  class="dual-input clean" type="date" value="<?php echo date('Y-m-d') ?>" placeholder="" id="spa_signed_date" name="spa_signed_date" >
    </div>
    <div class="dual-input-div second-dual-input">
    <p>Fullset Completed</p>
      <select class="dual-input clean" name="fullset_completed">
        <option value="">Please Select an Option</option>
        <option value="YES">Yes</option>
        <option value="NO">No</option>
      </select>
    </div>

    <div class="tempo-two-input-clear"></div>
    <div class="dual-input-div">
    <p>Cash Buyer</p>
      <select class="dual-input clean" name="cash_buyer" >
        <option value="">Please Select an Option</option>
        <option value="YES">Yes</option>
        <option value="NO">No</option>
      </select>
    </div>
    <div class="dual-input-div second-dual-input">
    <p>Cancelled Booking</p>
      <select class="dual-input clean" name="cancelled_booking" >
        <option value="">Please Select an Option</option>
        <option value="YES">Yes</option>
        <option value="NO">No</option>
      </select>
    </div>

    <div class="tempo-two-input-clear"></div>
    <div class="dual-input-div">
      <p>Case Status</p>
      <select class="dual-input clean" name="case_status" >
        <option value="">Please Select an Option</option>
        <option value="PENDING LO">Pending LO</option>
        <option value="PENDING LA">Pending LA</option>
        <option value="PENDING SPA">Pending SPA</option>
        <option value="PENDING Loan">Pending Loan</option>
        <option value="COMPLETED">Completed</option>
      </select>
    </div>
    <div class="dual-input-div second-dual-input">
    <p>Event or Personal</p>
      <select class="dual-input clean" name="event_personal">
        <option value="">Please Select an Option</option>
        <option value="EVENT">EVENT</option>
        <option value="PERSONAL">PERSONAL</option>
      </select>
    </div>

    <div class="tempo-two-input-clear"></div>
    <div class="dual-input-div">
      <p>Rate %</p>
      <input class="dual-input clean" type="text" id="rate" name="rate">
    </div>
    <div class="dual-input-div second-dual-input">
      <p>Agent Commission</p>
      <input class="dual-input clean" type="text" id="agent_comm" name="agent_comm">
      <!-- <input class="dual-input clean" type="text" placeholder="Calculate After Submit Rate" readonly> -->
    </div>

    <!-- <div class="tempo-two-input-clear"></div>
    <div class="dual-input-div">
      <p>UP1 Name</p>
      <input class="dual-input clean" type="text" placeholder="Auto Generated" id="upline1" name="upline1" readonly>
    </div>
    <div class="dual-input-div second-dual-input">
    <p>UP2 Name</p>
    <input  class="dual-input clean" type="text" placeholder="Auto Generated" id="upline2" name="upline2" readonly>
    </div> -->

    <div class="tempo-two-input-clear"></div>
    <div class="dual-input-div">
    <!-- <div class="dual-input-div second-dual-input"> -->
      <p>PL Name</p>
      <input  class="dual-input clean" type="text" placeholder="PL Name" id="pl_name" name="pl_name">
    </div>
    <div class="dual-input-div second-dual-input">
    <!-- <div class="tempo-two-input-clear"></div>
    <div class="dual-input-div"> -->
      <p>HOS Name</p>
      <input  class="dual-input clean" type="text" placeholder="HOS Name" id="hos_name" name="hos_name">
    </div>

    <div class="tempo-two-input-clear"></div>
    <div class="dual-input-div">
    <!-- <div class="dual-input-div second-dual-input"> -->
      <p>Lister Name</p>
      <input class="dual-input clean" type="text" placeholder="Lister Name" id="lister_name" name="lister_name">
    </div>

    <!-- <div class="tempo-two-input-clear"></div>
    <div class="dual-input-div"> -->
    <div class="dual-input-div second-dual-input">
      <p>UP1 Override</p>
      <input class="dual-input clean" type="text" id="ul_override" name="ul_override">
      <!-- <input class="dual-input clean" type="text" placeholder="Calculate After Submit Rate" readonly> -->
    </div>

    <div class="tempo-two-input-clear"></div>
    <div class="dual-input-div">
    <!-- <div class="dual-input-div second-dual-input"> -->
      <p>UP2 Override</p>
      <input class="dual-input clean" type="text" id="uul_override" name="uul_override">
      <!-- <input class="dual-input clean" type="text" placeholder="Calculate After Submit Rate" readonly> -->
    </div>

    <!-- <div class="tempo-two-input-clear"></div>
    <div class="dual-input-div"> -->
    <div class="dual-input-div second-dual-input">
      <p>PL Override</p>
      <!-- <input class="dual-input clean" type="text" placeholder="Calculate After Submit Rate" readonly> -->
      <input class="dual-input clean" type="text" id="pl_override" name="pl_override">

    </div>

    <div class="tempo-two-input-clear"></div>
    <div class="dual-input-div">
      <p>HOS Override</p>
      <!-- <input class="dual-input clean" type="text" placeholder="Calculate After Submit Rate" readonly> -->
      <input class="dual-input clean" type="text" id="hos_override" name="hos_override">
    </div>

    <div class="dual-input-div second-dual-input">
      <p>Lister Override</p>
      <input class="dual-input clean" type="text" id="lister_override" name="lister_override">
    </div>

    <div class="tempo-two-input-clear"></div>
    <div class="dual-input-div">
      <p>Admin1 Override</p>
      <input  class="dual-input clean" type="text" id="admin1_override" name="admin1_override" value="15">
    </div>
    <div class="dual-input-div second-dual-input">
      <p>Admin2 Override</p>
      <input class="dual-input clean" type="text" id="admin2_override" name="admin2_override" value="15">
    </div>

    <div class="tempo-two-input-clear"></div>
    <div class="dual-input-div">
      <p>Admin3 Override</p>
      <input class="dual-input clean" type="text" id="admin3_override" name="admin3_override" value="15">
    </div>
    <div class="dual-input-div second-dual-input">
      <p>GIC Profit</p>
      <input class="dual-input clean" type="text"  id="gic_profit" name="gic_profit">
    </div>

    <div class="tempo-two-input-clear"></div>
    <div class="dual-input-div">
      <p>Total Claimed Dev Amt</p>
      <input class="dual-input clean" type="text" id="total_claimed_dev_amt" name="total_claimed_dev_amt">
    </div>
    <div class="dual-input-div second-dual-input">
      <p>Total Balanced Dev Amt</p>
      <input class="dual-input clean" type="text" id="total_bal_unclaim_amt" name="total_bal_unclaim_amt">
    </div>
    <div class="tempo-two-input-clear"></div>

    <button input type="submit" name="upload" value="Upload" class="confirm-btn text-center white-text clean black-button">Confirm</button>

  </form>

</div>

<?php require_once dirname(__FILE__) . '/footer.php'; ?>
<?php include 'jsAdmin.php'; ?>

<?php
if(isset($_GET['type']))
{
    $messageType = null;

    if($_SESSION['messageType'] == 1)
    {
        if($_GET['type'] == 1)
        {
            $messageType = "New Product Added Successfully";
        }
        if($_GET['type'] == 2)
        {
            $messageType = "There is an error to add the new product";
        }

        echo '
        <script>
            putNoticeJavascript("Notice !! ","'.$messageType.'");
        </script>
        ';
        $_SESSION['messageType'] = 0;
    }
}
?>
<script>
$(document).ready(function(){
  var i = 0;
  $("#addBtn").click(function(){
    i++;
    var div = ('<div id="el'+i+'"><div class="dual-input-div"><p>Purchaser Name</p><input class="dual-input clean" type="text" placeholder="Purchaser Name" name="purchaser_name[]" required></div><div class="dual-input-div second-dual-input"><p>Contact</p><input class="dual-input clean" type="text" placeholder="Contact" id="contact" name="contact[]" required></div><div class="tempo-two-input-clear"></div><div class="dual-input-div"><p>IC</p><input class="dual-input clean" type="text" placeholder="IC" id="ic" name="ic[]" required></div><div class="dual-input-div second-dual-input"><p>E-mail</p><input class="dual-input clean" type="text" placeholder="E-mail" id="email" name="email[]"></div></div>');

    $(div).hide().appendTo("#addIn").slideDown(1000);
  });
  $("#remBtn").click(function(){
    if ($('#el'+i+' input').length > 1) {
                $('#el'+i+'').slideUp(1000, function(){
                  $(this).remove();
                });
                i--;
            }
  });
});
</script>
</body>
</html>
