-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jan 29, 2020 at 09:10 AM
-- Server version: 10.4.6-MariaDB
-- PHP Version: 7.3.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `vidatech_gic`
--

-- --------------------------------------------------------

--
-- Table structure for table `loan_status`
--

CREATE TABLE `loan_status` (
  `id` bigint(25) NOT NULL,
  `project_name` varchar(255) NOT NULL,
  `loan_uid` varchar(255) NOT NULL,
  `unit_no` varchar(255) NOT NULL,
  `purchaser_name` varchar(255) NOT NULL,
  `ic` varchar(255) NOT NULL,
  `contact` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `booking_date` varchar(255) DEFAULT NULL,
  `sq_ft` varchar(255) NOT NULL,
  `spa_price` varchar(255) NOT NULL,
  `package` varchar(255) NOT NULL,
  `discount` varchar(255) NOT NULL,
  `rebate` varchar(255) NOT NULL,
  `extra_rebate` varchar(255) NOT NULL,
  `nettprice` varchar(255) NOT NULL,
  `totaldevelopercomm` varchar(255) NOT NULL,
  `agent` varchar(255) NOT NULL,
  `loanstatus` varchar(255) NOT NULL,
  `remark` varchar(255) NOT NULL,
  `bform_Collected` varchar(255) NOT NULL,
  `payment_method` varchar(255) NOT NULL,
  `lawyer` varchar(255) NOT NULL,
  `pending_approval_status` varchar(255) NOT NULL,
  `bank_approved` varchar(255) NOT NULL,
  `lo_signed_date` varchar(255) NOT NULL,
  `la_signed_date` varchar(255) NOT NULL,
  `spa_signed_date` varchar(255) NOT NULL,
  `fullset_completed` varchar(255) NOT NULL,
  `cash_buyer` varchar(255) NOT NULL,
  `cancelled_booking` varchar(255) NOT NULL,
  `case_status` varchar(255) NOT NULL,
  `event_personal` varchar(255) NOT NULL,
  `rate` varchar(255) NOT NULL,
  `agent_comm` varchar(255) NOT NULL,
  `upline1` varchar(255) NOT NULL,
  `upline2` varchar(255) NOT NULL,
  `pl_name` varchar(255) NOT NULL,
  `hos_name` varchar(255) NOT NULL,
  `lister_name` varchar(255) NOT NULL,
  `ul_override` varchar(255) NOT NULL,
  `uul_override` varchar(255) NOT NULL,
  `pl_override` varchar(255) NOT NULL,
  `hos_override` varchar(255) NOT NULL,
  `lister_override` varchar(255) NOT NULL,
  `admin1_override` varchar(255) NOT NULL,
  `admin2_override` varchar(255) NOT NULL,
  `admin3_override` varchar(255) NOT NULL,
  `gic_profit` varchar(255) NOT NULL,
  `total_claimed_dev_amt` varchar(255) NOT NULL,
  `total_bal_unclaim_amt` varchar(255) NOT NULL,
  `pdf` varchar(255) DEFAULT NULL,
  `pdf_date` varchar(255) DEFAULT NULL,
  `1st_claim_invoice_no` varchar(255) NOT NULL,
  `1st_claim_amt` varchar(255) NOT NULL,
  `sst1` varchar(255) DEFAULT NULL,
  `request_date1` varchar(255) DEFAULT NULL,
  `check_no1` varchar(255) DEFAULT NULL,
  `receive_date1` varchar(255) DEFAULT NULL,
  `2nd_claim_invoice_no` varchar(255) NOT NULL,
  `2nd_claim_amt` varchar(255) NOT NULL,
  `sst2` varchar(255) DEFAULT NULL,
  `request_date2` varchar(255) DEFAULT NULL,
  `check_no2` varchar(255) DEFAULT NULL,
  `receive_date2` varchar(255) DEFAULT NULL,
  `3rd_claim_invoice_no` varchar(255) NOT NULL,
  `3rd_claim_amt` varchar(255) NOT NULL,
  `sst3` varchar(255) DEFAULT NULL,
  `request_date3` varchar(255) DEFAULT NULL,
  `check_no3` varchar(255) DEFAULT NULL,
  `receive_date3` varchar(255) DEFAULT NULL,
  `4th_claim_invoice_no` varchar(255) NOT NULL,
  `4th_claim_amt` varchar(255) NOT NULL,
  `sst4` varchar(25) DEFAULT NULL,
  `request_date4` varchar(255) DEFAULT NULL,
  `check_no4` varchar(255) DEFAULT NULL,
  `receive_date4` varchar(255) DEFAULT NULL,
  `5th_claim_invoice_no` varchar(255) NOT NULL,
  `5th_claim_amt` varchar(255) NOT NULL,
  `sst5` varchar(25) DEFAULT NULL,
  `request_date5` varchar(255) DEFAULT NULL,
  `check_no5` varchar(255) DEFAULT NULL,
  `receive_date5` varchar(255) DEFAULT NULL,
  `date_created` timestamp NOT NULL DEFAULT current_timestamp(),
  `date_updated` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `loan_status`
--

INSERT INTO `loan_status` (`id`, `project_name`, `loan_uid`, `unit_no`, `purchaser_name`, `ic`, `contact`, `email`, `booking_date`, `sq_ft`, `spa_price`, `package`, `discount`, `rebate`, `extra_rebate`, `nettprice`, `totaldevelopercomm`, `agent`, `loanstatus`, `remark`, `bform_Collected`, `payment_method`, `lawyer`, `pending_approval_status`, `bank_approved`, `lo_signed_date`, `la_signed_date`, `spa_signed_date`, `fullset_completed`, `cash_buyer`, `cancelled_booking`, `case_status`, `event_personal`, `rate`, `agent_comm`, `upline1`, `upline2`, `pl_name`, `hos_name`, `lister_name`, `ul_override`, `uul_override`, `pl_override`, `hos_override`, `lister_override`, `admin1_override`, `admin2_override`, `admin3_override`, `gic_profit`, `total_claimed_dev_amt`, `total_bal_unclaim_amt`, `pdf`, `pdf_date`, `1st_claim_invoice_no`, `1st_claim_amt`, `sst1`, `request_date1`, `check_no1`, `receive_date1`, `2nd_claim_invoice_no`, `2nd_claim_amt`, `sst2`, `request_date2`, `check_no2`, `receive_date2`, `3rd_claim_invoice_no`, `3rd_claim_amt`, `sst3`, `request_date3`, `check_no3`, `receive_date3`, `4th_claim_invoice_no`, `4th_claim_amt`, `sst4`, `request_date4`, `check_no4`, `receive_date4`, `5th_claim_invoice_no`, `5th_claim_amt`, `sst5`, `request_date5`, `check_no5`, `receive_date5`, `date_created`, `date_updated`) VALUES
(18, 'VIDA', 'b55a232199c6902b1f35fe63d0250ac5', '16-05', 'Pheh Guan Choon', '960720025125', '+602-7412369', 'mustaripro96@gmail.com', '2020-01-29', '1200', '1000', '131', '4', '5', '5', '1000', '1000', 'Ai Pheng', '', '', '', 'Developer Merchant', '', '', '', '', '', '', '', '', '', 'COMPLETED', '', '3', '30', 'Eddie Song', 'null', '', '', '', '1.5', '1.5', '4.5', '1.5', '0', '', '', '', '961', '100', '900', 'pdf', NULL, '', '100', 'NO', '29-01-2020', NULL, NULL, '', '106', 'YES', '29-01-2020', NULL, NULL, '', '', NULL, NULL, NULL, NULL, '', '', NULL, NULL, NULL, NULL, '', '', NULL, NULL, NULL, NULL, '2020-01-29 01:34:39', '2020-01-29 07:58:53');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `loan_status`
--
ALTER TABLE `loan_status`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `loan_status`
--
ALTER TABLE `loan_status`
  MODIFY `id` bigint(25) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
