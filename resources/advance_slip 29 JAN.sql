-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jan 29, 2020 at 09:10 AM
-- Server version: 10.4.6-MariaDB
-- PHP Version: 7.3.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `vidatech_gic`
--

-- --------------------------------------------------------

--
-- Table structure for table `advance_slip`
--

CREATE TABLE `advance_slip` (
  `id` int(255) NOT NULL,
  `unit_no` varchar(255) NOT NULL,
  `project_name` varchar(255) NOT NULL,
  `loan_uid` varchar(255) NOT NULL,
  `agent` varchar(255) NOT NULL,
  `amount` decimal(65,2) NOT NULL,
  `status` varchar(255) NOT NULL,
  `check_id` varchar(255) DEFAULT NULL,
  `receive_status` varchar(255) NOT NULL,
  `date_created` timestamp(1) NOT NULL DEFAULT current_timestamp(1) ON UPDATE current_timestamp(1)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `advance_slip`
--

INSERT INTO `advance_slip` (`id`, `unit_no`, `project_name`, `loan_uid`, `agent`, `amount`, `status`, `check_id`, `receive_status`, `date_created`) VALUES
(18, '16-05', 'VIDA', 'b55a232199c6902b1f35fe63d0250ac5', 'Ai Pheng', '2000.00', 'PENDING', NULL, 'PENDING', '2020-01-29 01:35:28.8');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `advance_slip`
--
ALTER TABLE `advance_slip`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `advance_slip`
--
ALTER TABLE `advance_slip`
  MODIFY `id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
