<?php

require_once dirname(__FILE__) . '/1dbCon/dbCon.php';
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/adminAccess1.php';
// require_once dirname(__FILE__) . '/classes/User.php';
require_once dirname(__FILE__) . '/classes/Product2.php';
require_once dirname(__FILE__) . '/classes/LoanStatus.php';
require_once dirname(__FILE__) . '/classes/Project.php';

require_once dirname(__FILE__) . '/utilities/allNoticeModals.php';
require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';
require_once dirname(__FILE__) . '/utilities/languageFunction.php';

$conn = connDB();
$projectName = "";
// $conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

?>

<!DOCTYPE html>
<html lang="en">
<head>
  <style>
table {
  border-spacing: 0;
  width: 100%;
  border: 1px solid #ddd;
}

th {
  cursor: pointer;
}

th, td {
  text-align: left;
  padding: 16px;
}

tr:nth-child(even) {
  background-color: #f2f2f2
}
</style>
    <meta charset="UTF-8">
    <?php require_once dirname(__FILE__) . '/header.php'; ?>
	<?php include 'meta.php'; ?>
    <!--<meta property="og:url" content="https://dcksupreme.asia/" />-->
    <meta property="og:title" content="Dashboard | GIC" />
    <title>Dashboard | GIC</title>
    <!--<link rel="canonical" href="https://dcksupreme.asia/" />-->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.0/css/bootstrap.min.css">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.0/css/bootstrap-theme.min.css">
    <?php include 'css.php'; ?>
</head>
<body class="body">
<?php  include 'admin1Header.php'; ?>


<?php echo '<script type="text/javascript" src="js/jquery-3.3.1.min.js"></script>'; ?>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.0/js/bootstrap.min.js"></script>

<div class="yellow-body same-padding">



    <h1 class="h1-title h1-before-border shipping-h1">Loan Status Form</h1>

    <div class="short-red-border"></div>
	<div class="width100 overflow section-divider">
        <a href="adminAddNewProject.php">
            <div class="five-red-btn-div">
                <p class="short-p five-red-p g-first-3-p n-p">NEW PROJECT</p>
            </div>
        </a>
        <a href="adminAddNewProduct.php">
            <div class="five-red-btn-div left-mid-red">
                <p class="short-p five-red-p f-first-3-p a-p">ADD NEW LOAN</p>
            </div>
        </a>
        <a href="#">
        <form method="post" action="export_data.php">
            <button class="five-red-btn-div clean clean-button" type="submit" name="export">
                <p class="short-p five-red-p e-first-3-p e-p">EXPORT TO EXCEL</p>
            </button>
        </form>
        </a>
    </div>
	<div class="clear"></div>
    <div class="section-divider width100 overflow">
		<?php $projectDetails = getProject($conn); ?>

    <form class="" action="selected.php" method="post">
      <select id="sel_id" name="admin1Product"  onchange="this.form.submit();" class="clean-select">
        <?php if (isset($_GET['name'])) {
          if ($_GET['name'] == 'SHOW ALL') {
            $projectName = "";
          }else {
            $type = $_GET['name'];
            $types = urldecode("$type");
            $projectName = "WHERE project_name = '$types'";
          }
          ?><option value=""><?php echo $_GET['name'] ?></option>
          <option value="">Choose Project</option><?php
        }else {
          ?><option value="">Choose Project</option><?php
        } ?>

        <?php if ($projectDetails) {
          for ($cnt=0; $cnt <count($projectDetails) ; $cnt++) {
            if ($projectDetails[$cnt]->getProjectName() != $types) {
              ?><option value="<?php echo $projectDetails[$cnt]->getProjectName()?>"><?php echo $projectDetails[$cnt]->getProjectName() ?></option><?php
              }
              }
              ?><option value="SHOW ALL">SHOW ALL</option><?php
            } ?>
      <!-- <option value="-1">Select</option>
      <option value="VIDA">kasper </option>
      <option value="VV">adad </option> -->
      <!-- <option value="14">3204 </option> -->
      </select>
    </form>


    </div>


    <!-- This is a filter for the table result -->
    <!-- <select class="filter-select clean">
    	<option class="filter-option">Latest Shipping</option>
        <option class="filter-option">Oldest Shipping</option>
    </select> -->
    <!-- End of Filter -->
    <div class="clear"></div>

    <div class="width100 shipping-div2">
        <?php $conn = connDB();?>
            <table id="myTable" class="shipping-table">
                <thead>
                    <tr>
                        <th onclick="sortTable(0)" class="th">NO.</th>
                        <th onclick="sortTable(0)" class="th"><?php echo wordwrap("UNIT NO.",7,"</br>\n");?></th>
                        <th onclick="sortTable(0)" class="th"><?php echo wordwrap("PURCHASER NAME",10,"</br>\n");?></th>
                        <th onclick="sortTable(0)" class="th"><?php echo wordwrap("BOOKING DATE",10,"</br>\n");?></th>
                        <th onclick="sortTable(0)" class="th"><?php echo wordwrap("SPA PRICE",8,"</br>\n");?></th>
                        <th onclick="sortTable(0)" class="th"><?php echo wordwrap("NETT PRICE",8,"</br>\n");?></th>
                        <th onclick="sortTable(0)" class="th">AGENT</th>
                        <th onclick="sortTable(0)" class="th"><?php echo wordwrap("LOAN STATUS",10,"</br>\n");?></th>
                        <th onclick="sortTable(0)" class="th"><?php echo wordwrap("SPA SIGNED DATE",10,"</br>\n");?></th>
                        <th onclick="sortTable(0)" class="th">CASH BUYER</th>
                        <th onclick="sortTable(0)" class="th"><?php echo wordwrap("CANCELLED BOOKING",10,"</br>\n");?></th>
                        <th onclick="sortTable(0)" class="th"><?php echo wordwrap("EVENT/PERSONAL",10,"</br>\n");?></th>
                        <th onclick="sortTable(0)" class="th">RATE</th>
                        <th onclick="sortTable(0)" class="th"><?php echo wordwrap("UPLINE 1",5,"</br>\n");?></th>
                        <th onclick="sortTable(0)" class="th"><?php echo wordwrap("UPLINE 2",5,"</br>\n");?></th>

                        <th onclick="sortTable(0)" class="th"><?php echo wordwrap("PL NAME",5,"</br>\n");?></th>
                        <th onclick="sortTable(0)" class="th"><?php echo wordwrap("HOS NAME",7,"</br>\n");?></th>
                        <th>ACTION</th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                    // for($cnt = 0;$cnt < count($productsOrders) ;$cnt++)
                    // {
                        $orderDetails = getLoanStatus($conn, $projectName);
                        if($orderDetails != null)
                        {
                            for($cntAA = 0;$cntAA < count($orderDetails) ;$cntAA++)
                            {?>
                            <tr>
                                <td class="td"><?php echo ($cntAA+1)?></td>
                                <td class="td"><?php echo $orderDetails[$cntAA]->getProjectName();?></td>
                                <td class="td"><?php echo $orderDetails[$cntAA]->getUnitNo();?></td>
                                <td class="td"><?php
                                $a = $orderDetails[$cntAA]->getPurchaserName();
                                 echo str_replace(',',"<br>", $a) ;?></td>
                                <!-- <td><?php //echo $orderDetails[$cntAA]->getStock();?></td> -->
                                <td class="td"><?php echo str_replace(',',"<br>", $orderDetails[$cntAA]->getIc());?></td>
                                <td class="td"><?php echo str_replace(',',"<br>", $orderDetails[$cntAA]->getContact());?></td>

                                <td class="td"><?php echo str_replace(',',"<br>", $orderDetails[$cntAA]->getEmail());?></td>
                                <td class="td"><?php echo date('d-m-Y', strtotime($orderDetails[$cntAA]->getBookingDate()));?></td>
                                <td class="td"><?php echo $orderDetails[$cntAA]->getSqFt();?></td>

                                <!-- <td class="td"><?php //echo $orderDetails[$cntAA]->getSpaPrice();?></td> -->

                                <!-- show , inside value -->
                                <?php $spaPrice = $orderDetails[$cntAA]->getSpaPrice();?>
                                <td class="td"><?php echo $spaPriceValue = number_format($spaPrice, 2); ?></td>

                                <td class="td"><?php echo $orderDetails[$cntAA]->getPackage();?></td>
                                <td class="td"><?php echo $orderDetails[$cntAA]->getDiscount();?></td>
                                <td class="td"><?php echo $orderDetails[$cntAA]->getRebate();?></td>
                                <td class="td"><?php echo $orderDetails[$cntAA]->getExtraRebate();?></td>

                                <!-- <td class="td"><?php //echo $orderDetails[$cntAA]->getNettPrice();?></td>
                                <td class="td"><?php //echo $orderDetails[$cntAA]->getTotalDeveloperComm();?></td> -->

                                <!-- show , inside value -->
                                <?php $nettPrice = $orderDetails[$cntAA]->getNettPrice();?>
                                <td class="td"><?php echo $nettPriceValue = number_format($nettPrice, 2); ?></td>
                                <?php $totalDevComm = $orderDetails[$cntAA]->getTotalDeveloperComm();?>
                                <td class="td"><?php echo $totalDevCommValue = number_format($totalDevComm, 2); ?></td>

                                <td class="td"><?php echo $orderDetails[$cntAA]->getAgent();?></td>
                                <td class="td"><?php echo wordwrap($orderDetails[$cntAA]->getLoanStatus(),50,"</br>\n");?></td>
                                <td class="td"><?php echo wordwrap($orderDetails[$cntAA]->getRemark(), 50, "</br>\n");?></td>
                                <td class="td"><?php echo $orderDetails[$cntAA]->getBFormCollected();?></td>
                                <td class="td"><?php echo $orderDetails[$cntAA]->getPaymentMethod();?></td>
                                <td class="td"><?php echo $orderDetails[$cntAA]->getLawyer();?></td>
                                <td class="td"><?php echo $orderDetails[$cntAA]->getPendingApprovalStatus();?></td>
                                <td class="td"><?php echo $orderDetails[$cntAA]->getBankApproved();?></td>
                                <td class="td"><?php echo $orderDetails[$cntAA]->getLoSignedDate();?></td>
                                <td class="td"><?php echo $orderDetails[$cntAA]->getLaSignedDate();?></td>
                                <td class="td"><?php echo $orderDetails[$cntAA]->getSpaSignedDate();?></td>
                                <td class="td"><?php echo $orderDetails[$cntAA]->getFullsetCompleted();?></td>

                                <td class="td"><?php echo $orderDetails[$cntAA]->getCashBuyer();?></td>

                                <td class="td"><?php echo $orderDetails[$cntAA]->getCancelledBooking();?></td>
                                <td class="td"><?php echo $orderDetails[$cntAA]->getCaseStatus();?></td>
                                <td class="td"><?php echo $orderDetails[$cntAA]->getEventPersonal();?></td>
                                <td class="td"><?php echo $orderDetails[$cntAA]->getRate();
                                if ($orderDetails[$cntAA]->getRate()) {
                                  echo "%";
                                }?></td>

                                <!-- <td class="td"><?php //echo $orderDetails[$cntAA]->getAgentComm();?></td> -->

                                <?php $agentComm = $orderDetails[$cntAA]->getAgentComm();?>
                                <td class="td"><?php echo $agentCommValue = number_format($agentComm, 2); ?></td>

                                <td class="td"><?php echo $orderDetails[$cntAA]->getUpline1();?></td>
                                <td class="td"><?php echo $orderDetails[$cntAA]->getUpline2();?></td>
                                <td class="td"><?php echo $orderDetails[$cntAA]->getPlName();?></td>
                                <td class="td"><?php echo $orderDetails[$cntAA]->getHosName();?></td>
                                <td class="td"><?php echo $orderDetails[$cntAA]->getListerName();?></td>

                                <!-- <td class="td"><?php //echo $orderDetails[$cntAA]->getUlOverride();?></td>
                                <td class="td"><?php //echo $orderDetails[$cntAA]->getUulOverride();?></td>
                                <td class="td"><?php //echo $orderDetails[$cntAA]->getPlOverride();?></td>
                                <td class="td"><?php //echo $orderDetails[$cntAA]->getHosOverride();?></td>
                                <td class="td"><?php //echo $orderDetails[$cntAA]->getListerOverride();?></td> -->

                                <!-- show , inside value -->
                                <?php $ulOverride = $orderDetails[$cntAA]->getUlOverride();?>
                                <td class="td"><?php echo $ulOverrideValue = number_format($ulOverride, 2); ?></td>
                                <?php $uUlOverride = $orderDetails[$cntAA]->getUulOverride();?>
                                <td class="td"><?php echo $uUlOverrideValue = number_format($uUlOverride, 2); ?></td>
                                <?php $plOverride = $orderDetails[$cntAA]->getPlOverride();?>
                                <td class="td"><?php echo $plOverrideValue = number_format($plOverride, 2); ?></td>
                                <?php $hosOverride = $orderDetails[$cntAA]->getHosOverride();?>
                                <td class="td"><?php echo $hosOverrideValue = number_format($hosOverride, 2); ?></td>
                                <?php $listerOverride = $orderDetails[$cntAA]->getListerOverride();?>
                                <td class="td"><?php echo $listerOverrideValue = number_format($listerOverride, 2); ?></td>


                                <!-- <td class="td"><?php //echo $orderDetails[$cntAA]->getAdmin1Override();?></td>
                                <td class="td"><?php //echo $orderDetails[$cntAA]->getAdmin2Override();?></td>
                                <td class="td"><?php //echo $orderDetails[$cntAA]->getAdmin3Override();?></td> -->

                                <!-- show , inside value -->
                                <?php $a1Over = $orderDetails[$cntAA]->getAdmin1Override();?>
                                <td class="td"><?php echo $a3OverValue = number_format($a1Over, 2); ?></td>
                                <?php $a2Over = $orderDetails[$cntAA]->getAdmin2Override();?>
                                <td class="td"><?php echo $a2OverValue = number_format($a2Over, 2); ?></td>
                                <?php $a3Over = $orderDetails[$cntAA]->getAdmin3Override();?>
                                <td class="td"><?php echo $a3OverValue = number_format($a3Over, 2); ?></td>


                                <!-- <td class="td"><?php //echo $orderDetails[$cntAA]->getGicProfit();?></td>
                                <td class="td"><?php //echo $orderDetails[$cntAA]->getTotalClaimDevAmt();?></td>
                                <td class="td"><?php //echo $orderDetails[$cntAA]->getTotalBalUnclaimAmt();?></td> -->
                                <!-- show , inside value -->
                                <?php $gicProfit = $orderDetails[$cntAA]->getGicProfit();?>
                                <td class="td"><?php echo $gicProfitValue = number_format($gicProfit, 2); ?></td>
                                <?php $totalClaim = $orderDetails[$cntAA]->getTotalClaimDevAmt();?>
                                <td class="td"><?php echo $totalClaimValue = number_format($totalClaim, 2); ?></td>
                                <?php $totalUnclaim = $orderDetails[$cntAA]->getTotalBalUnclaimAmt();?>
                                <td class="td"><?php echo $totalUnclaimValue = number_format($totalUnclaim, 2); ?></td>

                                <td class="td">
                                    <form action="editProduct.php" method="POST">
                                        <button class="clean edit-anc-btn hover1" type="submit" name="loan_uid" value="<?php echo $orderDetails[$cntAA]->getLoanUid();?>">
                                            <img src="img/edit.png" class="edit-announcement-img hover1a" alt="Edit Product" title="Edit Product">
                                            <img src="img/edit3.png" class="edit-announcement-img hover1b" alt="Edit Product" title="Edit Product">
                                        </button>
                                    </form>
                                </td>
                            </tr>
                            <?php
                            }
                        }
                    //}
                    ?>
                </tbody>
            </table><br>


    </div>


    <?php $conn->close();?>

</div>




<?php require_once dirname(__FILE__) . '/footer.php'; ?>
<?php include 'jsAdmin.php'; ?>

<?php
if(isset($_GET['type']))
{
    $messageType = null;

    if($_SESSION['messageType'] == 1)
    {
        if($_GET['type'] == 1)
        {
            $messageType = "Server currently fail. Please try again later.";
        }
        else if($_GET['type'] == 2)
        {
            $messageType = "Successfully Delete Product.";
        }
        else if($_GET['type'] == 3)
        {
            $messageType = "Error Deleting Product";
        }
        else if($_GET['type'] == 4)
        {
            $messageType = "New Project Created Successfully !";
        }
        echo '
        <script>
            putNoticeJavascript("Notice !! ","'.$messageType.'");
        </script>
        ';
        $_SESSION['messageType'] = 0;
    }
}
?>
<script>
$(function () {
    $('.link-to-details').click(function () {
        window.location.href = $(this).data('url');
    });
})

</script>
<script>
function sortTable(n) {
var table, rows, switching, i, x, y, shouldSwitch, dir, switchcount = 0;
table = document.getElementById("myTable");
switching = true;
//Set the sorting direction to ascending:
dir = "asc";
/*Make a loop that will continue until
no switching has been done:*/
while (switching) {
  //start by saying: no switching is done:
  switching = false;
  rows = table.rows;
  /*Loop through all table rows (except the
  first, which contains table headers):*/
  for (i = 1; i < (rows.length - 1); i++) {
    //start by saying there should be no switching:
    shouldSwitch = false;
    /*Get the two elements you want to compare,
    one from current row and one from the next:*/
    x = rows[i].getElementsByTagName("TD")[n];
    y = rows[i + 1].getElementsByTagName("TD")[n];
    /*check if the two rows should switch place,
    based on the direction, asc or desc:*/
    if (dir == "asc") {
      if (x.innerHTML.toLowerCase() > y.innerHTML.toLowerCase()) {
        //if so, mark as a switch and break the loop:
        shouldSwitch= true;
        break;
      }
    } else if (dir == "desc") {
      if (x.innerHTML.toLowerCase() < y.innerHTML.toLowerCase()) {
        //if so, mark as a switch and break the loop:
        shouldSwitch = true;
        break;
      }
    }
  }
  if (shouldSwitch) {
    /*If a switch has been marked, make the switch
    and mark that a switch has been done:*/
    rows[i].parentNode.insertBefore(rows[i + 1], rows[i]);
    switching = true;
    //Each time a switch is done, increase this count by 1:
    switchcount ++;
  } else {
    /*If no switching has been done AND the direction is "asc",
    set the direction to "desc" and run the while loop again.*/
    if (switchcount == 0 && dir == "asc") {
      dir = "desc";
      switching = true;
    }
  }
}
}
</script>
</body>
</html>
