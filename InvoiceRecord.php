<?php

require_once dirname(__FILE__) . '/1dbCon/dbCon.php';
require_once dirname(__FILE__) . '/adminAccess2.php';
require_once dirname(__FILE__) . '/sessionLoginChecker.php';

require_once dirname(__FILE__) . '/classes/Invoice.php';
require_once dirname(__FILE__) . '/classes/LoanStatus.php';

require_once dirname(__FILE__) . '/utilities/allNoticeModals.php';
require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';

$conn = connDB();

$invoiceDetails = getInvoice($conn);
$projectName = "";
// $conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <?php require_once dirname(__FILE__) . '/header.php'; ?>
	<?php include 'meta.php'; ?>
    <!--<meta property="og:url" content="https://dcksupreme.asia/" />-->
    <meta property="og:title" content="Invoice Status | GIC" />
    <title>Invoice Status | GIC</title>
    <!--<link rel="canonical" href="https://dcksupreme.asia/" />-->
    <?php include 'css.php'; ?>
</head>
<body class="body">
<?php //include 'header-admin.php'; ?>
<?php  include 'admin2Header.php'; ?>

<?php echo '<script type="text/javascript" src="js/jquery-3.3.1.min.js"></script>'; ?>

<div class="yellow-body same-padding">

    <h1 class="h1-title h1-before-border shipping-h1">Invoice Status</h1>

    <div class="short-red-border"></div>

    <!-- <div class="width100 overflow section-divider">
        <div class="three-input-div dual-input-div">
            <p>Column</p>
                <select required class="dual-input clean bigger-input">
                    <option>Invoice No.</option>
                    <option>Attn.</option>
                    <option>Project</option>
                    <option>Status</option>
                    <option>Date</option>
                    <option>Amount</option>
                </select>
        </div>

        <div class="three-input-div dual-input-div second-three-input">
            <p>Keyword</p>
            <input required class="dual-input clean bigger-input" type="text" placeholder="Keyword">
        </div>

        <div class="three-input-div dual-input-div">
            <button class="red-btn clean three-input-btn bigger-input">Search</button>
        </div>
    </div>

    <div class="clear"></div> -->

    <div class="float-right section-divider mobile-100">

    <form class="" action="" method="post">
              <select id="sel_id" name="sel_name"  onchange="this.form.submit();" class="clean-select">
                <option value="">Choose Status</option>

                <!-- <option value=" WHERE `status`='Pending'">Pending</option>
                <option value=" WHERE `status`='Issued'">Issued</option>
                <option value=" WHERE `status`='Rejected'">Rejected</option> -->

                <option value=" WHERE `receive_status`='Pending'">Pending</option>
                <option value=" WHERE `receive_status`='Issued'">Issued</option>
                <option value=" WHERE `receive_status`='Rejected'">Rejected</option>

                <option value="">Show All</option>
              </select>
            </form>

        <?php
            if (isset($_POST['sel_name']))
            {
                $projectName =  $_POST['sel_name'];
            }
            else
            {
                // echo "string";
            }
        ?>


    </div>

    <div class="clear"></div>

    <div class="width100 shipping-div2">
        <table class="shipping-table">
            <thead>
                <tr>
                    <th class="th">NO.</th>
                    <th class="th">Invoice No.</th>
                    <th class="th">Attn.</th>
                    <th class="th">Project</th>
                    <th class="th">Status</th>
                    <th class="th">Date</th>
                    <th class="th">Amount</th>
                    <th class="th">Payment Status</th>
                </tr>
            </thead>

            <tbody>
                <?php

                    // $conn = connDB();
                    $invoiceDetails = getInvoice($conn, $projectName);
                    if($invoiceDetails != null)
                    {
                        for($cntAA = 0;$cntAA < count($invoiceDetails) ;$cntAA++)
                        {
                        ?>
                            <tr>
                                <td class="td"><?php echo ($cntAA+1)?></td>

                                <td class="td">
                                    <?php echo date('Ymd', strtotime($invoiceDetails[$cntAA]->getDateCreated()));?>/
                                    <?php echo $invoiceDetails[$cntAA]->getProjectName();?>/
                                    <?php echo $invoiceDetails[$cntAA]->getID();?>
                                </td>

                                <td class="td"><?php echo $invoiceDetails[$cntAA]->getProjectHandler();?></td>
                                <td class="td"><?php echo $invoiceDetails[$cntAA]->getProjectName();?></td>
                                <td class="td"><?php echo $invoiceDetails[$cntAA]->getClaimsStatus();?></td>

                                <td class="td"><?php echo date('d-m-Y', strtotime($invoiceDetails[$cntAA]->getDateCreated()));?></td>
                                <!-- <td class="td"><?php //echo $invoiceDetails[$cntAA]->getDateCreated();?></td> -->

                                <td class="td"><?php echo $invoiceDetails[$cntAA]->getFinalAmount();?></td>
                                <td class="td"><?php echo $invoiceDetails[$cntAA]->getReceiveStatus();?></td>
                            </tr>
                        <?php
                        }
                    }
                    // $conn->close();
                ?>
            </tbody>
        </table>
    </div>

</div>

<?php require_once dirname(__FILE__) . '/footer.php'; ?>
<?php include 'jsAdmin.php'; ?>

</body>
</html>
