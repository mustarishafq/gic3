<?php

require_once dirname(__FILE__) . '/1dbCon/dbCon.php';
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/adminAccess2.php';
require_once dirname(__FILE__) . '/classes/Invoice.php';
require_once dirname(__FILE__) . '/classes/Product2.php';
require_once dirname(__FILE__) . '/classes/LoanStatus.php';
require_once dirname(__FILE__) . '/classes/Project.php';

require_once dirname(__FILE__) . '/utilities/allNoticeModals.php';
require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';
require_once dirname(__FILE__) . '/utilities/languageFunction.php';

$conn = connDB();

$loanDetails = getLoanStatus($conn, "WHERE case_status = 'COMPLETED'");
$invoiceDetails = getInvoice($conn);
$projectName = "WHERE case_status = 'COMPLETED'";
// $conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <?php require_once dirname(__FILE__) . '/header.php'; ?>
	<?php include 'meta.php'; ?>
    <!--<meta property="og:url" content="https://dcksupreme.asia/" />-->
    <meta property="og:title" content="Invoice Record | GIC" />
    <title>Invoice Record | GIC</title>
    <!--<link rel="canonical" href="https://dcksupreme.asia/" />-->
    <?php include 'css.php'; ?>
</head>
<body class="body">
<?php //include 'header-admin.php'; ?>
<?php  include 'admin2Header.php'; ?>

<?php echo '<script type="text/javascript" src="js/jquery-3.3.1.min.js"></script>'; ?>

<div class="yellow-body same-padding">
	<h1 class="h1-title h1-before-border shipping-h1">Invoice Follow-up</h1>
    <div class="short-red-border"></div>
    <!-- This is a filter for the table result -->
	<div class="clean"></div>
    <!-- <div  class="section-divider">
		<?php //$projectDetails = getProject($conn); ?>

            <form class="" action="" method="post">




    <!-- End of Filter -->
    <div class="clear"></div><br>

    <div >
        <?php $conn = connDB();?>
            <table class="shipping-table">
                <thead>
                    <tr>
                        <!-- <th class="th">NO.</th> -->
                        <th class="th"><?php echo wordwrap("PROJECT NAME",7,"</br>\n");?></th>
                        <th class="th"><?php echo wordwrap("UNIT NO.",7,"</br>\n");?></th>
                        <th class="th">DATE</th>
                        <th class="th"><?php echo wordwrap("INVOICE NO.",10,"</br>\n");?></th>
                        <th class="th">DETAILS</th>
                        <th class="th">AMOUNT (RM)</th>
                        <th class="th">SST</th>
                        <th class="th">1ST STATUS</th>
                        <th class="th">RECEIVED DATE</th>
                        <th class="th">CHECK NO.</th>
                        <th class="th">ACTION</th>

                        <!-- <th>STOCK</th> -->


                        <!-- <th class="th">BANK APPROVED</th>
                        <th class="th">LO SIGNED DATE</th>
                        <th class="th">LA SIGNED DATE</th>
                        <th class="th">LA SIGNED DATE</th>
                        <th class="th">LA SIGNED DATE</th>
                        <th class="th">LA SIGNED DATE</th> -->
                        <!-- <th>INVOICE</th> -->
                    </tr>
                </thead>
                <tbody>
                    <?php
                    // if ($invoiceDetails) {

                        $orderDetails = getLoanStatus($conn);
                        if($orderDetails != null)
                        {
                            for($cntAA = 0;$cntAA < count($orderDetails) ;$cntAA++)
                            {
                              $proDetails = getProject($conn, "WHERE project_name=?",array("project_name"),array($orderDetails[$cntAA]->getProjectName()), "s");
?>
                            <tr>
                              <?php if (!$orderDetails[$cntAA]->getCheckNo1() && $proDetails[0]->getProjectClaims() >= 1 && $orderDetails[$cntAA]->getClaimAmt1st()) {
                                ?>
                                <!-- <td class="td"><?php //echo $cntAA + 1?></td> -->
                                <td class="td"><?php echo $orderDetails[$cntAA]->getProjectName();?></td>
                                <td class="td"><?php echo $orderDetails[$cntAA]->getUnitNo();?></td>
                                <td class="td"><?php echo $orderDetails[$cntAA]->getRequestDate1();?></td>
                                <td class="td"><?php echo date('ymd',strtotime($orderDetails[$cntAA]->getRequestDate1()))."1".$orderDetails[$cntAA]->getId();?></td>
                                <td class="td">1st Commission</td>
                                <td class="td"><?php echo $orderDetails[$cntAA]->getClaimAmt1st();?></td>
                                <td class="td"><?php echo $orderDetails[$cntAA]->getSst1();?></td>
                                <?php if (!$orderDetails[$cntAA]->getCheckNo1() && $orderDetails[$cntAA]->getRequestDate1()) {
                                  ?><td class="td">PENDING</td><?php
                                }elseif (!$orderDetails[$cntAA]->getRequestDate1()) {
                                  ?><td class="td"></td><?php
                                }else {
                                  ?><td class="td">COMPLETED</td><?php
                                } ?>
                                <td class="td"><?php echo $orderDetails[$cntAA]->getReceiveDate1();?></td>
                                <td class="td">
                                  <?php if (!$orderDetails[$cntAA]->getCheckNo1()) {
                                ?><form class="" action="utilities/checkNo.php" method="post">
                                  <?php if ($orderDetails[$cntAA]->getCheckNo1()) {
                                    echo $orderDetails[$cntAA]->getCheckNo1();
                                  }else {
                                    ?>
                                    <input required class="no-outline" type="text" name="check_id" placeholder="CHECK NUMBER" value="<?php echo $orderDetails[$cntAA]->getCheckNo1();?>">
                                    <button class="button-add" type="submit" name="addCheckID1">Submit</button>
                                  <?php } ?>
                                  <!-- <input type="hidden" name="loan_uid" value="<?php //echo $orderDetails[$cntAA]->getLoanUid() ?>"> -->
                                  <input type="hidden" name="id" value="<?php echo $orderDetails[$cntAA]->getID() ?>">

                                  <button class="button-remove" type="submit" name="removeCheckID1">Delete</button>
                                </form>
                              <?php } ?>
                              </td>
                              <td class="td">
                                <?php if ($orderDetails[$cntAA]->getClaimAmt1st()) {
                                  ?><form action="invoice.php" method="POST">
                                    <a><button class="clean edit-anc-btn hover1" style="color: blue" type="submit" name="loan_uid" value="<?php echo $orderDetails[$cntAA]->getLoanUid();?>">Print Invoice
                                      <input type="hidden" name="claim_status" value="1">
                                      <input type="hidden" name="invoice_no" value="<?php echo date('ymd',strtotime($orderDetails[$cntAA]->getRequestDate1()))."1".$orderDetails[$cntAA]->getId();?>">
                                      <input type="hidden" name="request_date" value="<?php echo $orderDetails[$cntAA]->getRequestDate1()?>">
                                          <!-- <img src="img/edit.png" class="edit-announcement-img hover1a" alt="Edit Product" title="Edit Product"> -->
                                          <!-- <img src="img/edit3.png" class="edit-announcement-img hover1b" alt="Edit Product" title="Edit Product"> -->
                                      </button></a>
                                  </form><?php
                                }else {
                                  if ($proDetails[0]->getProjectClaims() >= 1 ) {
                                  ?><form action="editInvoice.php" method="POST">
                                    <a><button class="clean edit-anc-btn hover1" style="color: blue" type="submit" name="loan_uid" value="<?php echo $orderDetails[$cntAA]->getLoanUid();?>">Issue Invoice
                                      <input type="hidden" name="claim_status" value="1">
                                      <input type="hidden" name="invoice_no" value="<?php echo date('ymd',strtotime($orderDetails[$cntAA]->getRequestDate1()))."1".$orderDetails[$cntAA]->getId();?>">
                                      <input type="hidden" name="request_date" value="<?php echo $orderDetails[$cntAA]->getRequestDate1()?>">
                                          <!-- <img src="img/edit.png" class="edit-announcement-img hover1a" alt="Edit Product" title="Edit Product"> -->
                                          <!-- <img src="img/edit3.png" class="edit-announcement-img hover1b" alt="Edit Product" title="Edit Product"> -->
                                      </button></a>
                                  </form><?php
                                }} ?><?php
                              } ?>
                                <!-- <td><?php //echo ($cntAA+1)?></td> -->
                            </tr>
                            <tr>
                              <?php if (!$orderDetails[$cntAA]->getCheckNo2() && $proDetails[0]->getProjectClaims() >= 2 && $orderDetails[$cntAA]->getClaimAmt2nd()) {
                                ?>
                                <!-- <td class="td"><?php //echo $cntAA + 1?></td> -->
                                <td class="td"><?php echo $orderDetails[$cntAA]->getProjectName();?></td>
                                <td class="td"><?php echo $orderDetails[$cntAA]->getUnitNo();?></td>
                                <td class="td"><?php echo $orderDetails[$cntAA]->getRequestDate2();?></td>
                                <td class="td"><?php echo date('ymd',strtotime($orderDetails[$cntAA]->getRequestDate2()))."2".$orderDetails[$cntAA]->getId();?></td>
                                <td class="td">2nd Commission</td>
                                <td class="td"><?php echo $orderDetails[$cntAA]->getClaimAmt2nd();?></td>
                                <td class="td"><?php echo $orderDetails[$cntAA]->getSst2();?></td>
                                <?php if (!$orderDetails[$cntAA]->getCheckNo2() && $orderDetails[$cntAA]->getRequestDate2()) {
                                  ?><td class="td">PENDING</td><?php
                                }elseif (!$orderDetails[$cntAA]->getRequestDate2()) {
                                  ?><td class="td"></td><?php
                                }else {
                                  ?><td class="td">COMPLETED</td><?php
                                } ?>
                                <td class="td"><?php echo $orderDetails[$cntAA]->getReceiveDate2();?></td>
                                <td class="td">
                                  <?php if (!$orderDetails[$cntAA]->getCheckNo2()) {
                                ?><form class="" action="utilities/checkNo.php" method="post">
                                  <?php if ($orderDetails[$cntAA]->getCheckNo2()) {
                                    echo $orderDetails[$cntAA]->getCheckNo2();
                                  }else {
                                    ?>
                                    <input required class="no-outline" type="text" name="check_id" placeholder="CHECK NUMBER" value="<?php echo $orderDetails[$cntAA]->getCheckNo2();?>">
                                    <button class="button-add" type="submit" name="addCheckID2">Submit</button>
                                  <?php } ?>
                                  <!-- <input type="hidden" name="loan_uid" value="<?php //echo $orderDetails[$cntAA]->getLoanUid() ?>"> -->
                                  <input type="hidden" name="id" value="<?php echo $orderDetails[$cntAA]->getID() ?>">

                                  <button class="button-remove" type="submit" name="removeCheckID2">Delete</button>
                                </form>
                              <?php } ?>
                              </td>
                              <td class="td">
                                <?php if ($orderDetails[$cntAA]->getClaimAmt2nd()) {
                                  ?><form action="invoice.php" method="POST">
                                    <a><button class="clean edit-anc-btn hover1" style="color: blue" type="submit" name="loan_uid" value="<?php echo $orderDetails[$cntAA]->getLoanUid();?>">Print Invoice
                                      <input type="hidden" name="claim_status" value="2">
                                      <input type="hidden" name="invoice_no" value="<?php echo date('ymd',strtotime($orderDetails[$cntAA]->getRequestDate2()))."2".$orderDetails[$cntAA]->getId();?>">
                                      <input type="hidden" name="request_date" value="<?php echo $orderDetails[$cntAA]->getRequestDate2()?>">
                                          <!-- <img src="img/edit.png" class="edit-announcement-img hover1a" alt="Edit Product" title="Edit Product"> -->
                                          <!-- <img src="img/edit3.png" class="edit-announcement-img hover1b" alt="Edit Product" title="Edit Product"> -->
                                      </button></a>
                                  </form><?php
                                }else {
                                  if ($proDetails[0]->getProjectClaims() >= 2 ) {
                                  ?><form action="editInvoice.php" method="POST">
                                    <a><button class="clean edit-anc-btn hover1" style="color: blue" type="submit" name="loan_uid" value="<?php echo $orderDetails[$cntAA]->getLoanUid();?>">Issue Invoice
                                      <input type="hidden" name="claim_status" value="2">
                                      <input type="hidden" name="invoice_no" value="<?php echo date('ymd',strtotime($orderDetails[$cntAA]->getRequestDate2()))."2".$orderDetails[$cntAA]->getId();?>">
                                      <input type="hidden" name="request_date" value="<?php echo $orderDetails[$cntAA]->getRequestDate2()?>">
                                          <!-- <img src="img/edit.png" class="edit-announcement-img hover1a" alt="Edit Product" title="Edit Product"> -->
                                          <!-- <img src="img/edit3.png" class="edit-announcement-img hover1b" alt="Edit Product" title="Edit Product"> -->
                                      </button></a>
                                  </form><?php
                                }} ?><?php
                              } ?>
                            </tr>
                            <tr>
                              <?php if (!$orderDetails[$cntAA]->getCheckNo3() && $proDetails[0]->getProjectClaims() >= 3 && $orderDetails[$cntAA]->getClaimAmt3rd()) {
                                ?>
                                <!-- <td class="td"><?php //echo $cntAA + 1?></td> -->
                                <td class="td"><?php echo $orderDetails[$cntAA]->getProjectName();?></td>
                                <td class="td"><?php echo $orderDetails[$cntAA]->getUnitNo();?></td>
                                <td class="td"><?php echo $orderDetails[$cntAA]->getRequestDate3();?></td>
                                <td class="td"><?php echo date('ymd',strtotime($orderDetails[$cntAA]->getRequestDate3()))."3".$orderDetails[$cntAA]->getId();?></td>
                                <td class="td">3rd Commission</td>
                                <td class="td"><?php echo $orderDetails[$cntAA]->getClaimAmt3rd();?></td>
                                <td class="td"><?php echo $orderDetails[$cntAA]->getSst3();?></td>
                                <?php if (!$orderDetails[$cntAA]->getCheckNo3() && $orderDetails[$cntAA]->getRequestDate3()) {
                                  ?><td class="td">PENDING</td><?php
                                }elseif (!$orderDetails[$cntAA]->getRequestDate3()) {
                                  ?><td class="td"></td><?php
                                }else {
                                  ?><td class="td">COMPLETED</td><?php
                                } ?>
                                <td class="td"><?php echo $orderDetails[$cntAA]->getReceiveDate3();?></td>
                                <td class="td">
                                  <?php if (!$orderDetails[$cntAA]->getCheckNo3()) {
                                ?><form class="" action="utilities/checkNo.php" method="post">
                                  <?php if ($orderDetails[$cntAA]->getCheckNo3()) {
                                    echo $orderDetails[$cntAA]->getCheckNo3();
                                  }else {
                                    ?>
                                    <input required class="no-outline" type="text" name="check_id" placeholder="CHECK NUMBER" value="<?php echo $orderDetails[$cntAA]->getCheckNo3();?>">
                                    <button class="button-add" type="submit" name="addCheckID3">Submit</button>
                                  <?php } ?>
                                  <!-- <input type="hidden" name="loan_uid" value="<?php //echo $orderDetails[$cntAA]->getLoanUid() ?>"> -->
                                  <input type="hidden" name="id" value="<?php echo $orderDetails[$cntAA]->getID() ?>">

                                  <button class="button-remove" type="submit" name="removeCheckID3">Delete</button>
                                </form>
                              <?php } ?>
                              </td>
                              <td class="td">
                                <?php if ($orderDetails[$cntAA]->getClaimAmt3rd()) {
                                  ?><form action="invoice.php" method="POST">
                                    <a><button class="clean edit-anc-btn hover1" style="color: blue" type="submit" name="loan_uid" value="<?php echo $orderDetails[$cntAA]->getLoanUid();?>">Print Invoice
                                      <input type="hidden" name="claim_status" value="3">
                                      <input type="hidden" name="invoice_no" value="<?php echo date('ymd',strtotime($orderDetails[$cntAA]->getRequestDate3()))."3".$orderDetails[$cntAA]->getId();?>">
                                      <input type="hidden" name="request_date" value="<?php echo $orderDetails[$cntAA]->getRequestDate3()?>">
                                          <!-- <img src="img/edit.png" class="edit-announcement-img hover1a" alt="Edit Product" title="Edit Product"> -->
                                          <!-- <img src="img/edit3.png" class="edit-announcement-img hover1b" alt="Edit Product" title="Edit Product"> -->
                                      </button></a>
                                  </form><?php
                                }else {
                                  if ($proDetails[0]->getProjectClaims() >= 3 ) {
                                  ?><form action="editInvoice.php" method="POST">
                                    <a><button class="clean edit-anc-btn hover1" style="color: blue" type="submit" name="loan_uid" value="<?php echo $orderDetails[$cntAA]->getLoanUid();?>">Issue Invoice
                                      <input type="hidden" name="claim_status" value="3">
                                      <input type="hidden" name="invoice_no" value="<?php echo date('ymd',strtotime($orderDetails[$cntAA]->getRequestDate3()))."3".$orderDetails[$cntAA]->getId();?>">
                                      <input type="hidden" name="request_date" value="<?php echo $orderDetails[$cntAA]->getRequestDate3()?>">
                                          <!-- <img src="img/edit.png" class="edit-announcement-img hover1a" alt="Edit Product" title="Edit Product"> -->
                                          <!-- <img src="img/edit3.png" class="edit-announcement-img hover1b" alt="Edit Product" title="Edit Product"> -->
                                      </button></a>
                                  </form><?php
                                }} ?><?php
                              } ?>
                            </tr>
                            <tr>
                              <?php if (!$orderDetails[$cntAA]->getCheckNo4() && $proDetails[0]->getProjectClaims() >= 4 && $orderDetails[$cntAA]->getClaimAmt4th()) {
                                ?>
                                <!-- <td class="td"><?php //echo $cntAA + 1?></td> -->
                                <td class="td"><?php echo $orderDetails[$cntAA]->getProjectName();?></td>
                                <td class="td"><?php echo $orderDetails[$cntAA]->getUnitNo();?></td>
                                <td class="td"><?php echo $orderDetails[$cntAA]->getRequestDate4();?></td>
                                <td class="td"><?php echo date('ymd',strtotime($orderDetails[$cntAA]->getRequestDate4()))."4".$orderDetails[$cntAA]->getId();?></td>
                                <td class="td">4th Commission</td>
                                <td class="td"><?php echo $orderDetails[$cntAA]->getClaimAmt4th();?></td>
                                <td class="td"><?php echo $orderDetails[$cntAA]->getSst4();?></td>
                                <?php if (!$orderDetails[$cntAA]->getCheckNo4() && $orderDetails[$cntAA]->getRequestDate4()) {
                                  ?><td class="td">PENDING</td><?php
                                }elseif (!$orderDetails[$cntAA]->getRequestDate4()) {
                                  ?><td class="td"></td><?php
                                }else {
                                  ?><td class="td">COMPLETED</td><?php
                                } ?>
                                <td class="td"><?php echo $orderDetails[$cntAA]->getReceiveDate4();?></td>
                                <td class="td">
                                  <?php if (!$orderDetails[$cntAA]->getCheckNo4()) {
                                ?><form class="" action="utilities/checkNo.php" method="post">
                                  <?php if ($orderDetails[$cntAA]->getCheckNo4()) {
                                    echo $orderDetails[$cntAA]->getCheckNo4();
                                  }else {
                                    ?>
                                    <input required class="no-outline" type="text" name="check_id" placeholder="CHECK NUMBER" value="<?php echo $orderDetails[$cntAA]->getCheckNo4();?>">
                                    <button class="button-add" type="submit" name="addCheckID4">Submit</button>
                                  <?php } ?>
                                  <!-- <input type="hidden" name="loan_uid" value="<?php //echo $orderDetails[$cntAA]->getLoanUid() ?>"> -->
                                  <input type="hidden" name="id" value="<?php echo $orderDetails[$cntAA]->getID() ?>">

                                  <button class="button-remove" type="submit" name="removeCheckID4">Delete</button>
                                </form>
                              <?php } ?>
                              </td>
                              <td class="td">
                                <?php if ($orderDetails[$cntAA]->getClaimAmt4th()) {
                                  ?><form action="invoice.php" method="POST">
                                    <a><button class="clean edit-anc-btn hover1" style="color: blue" type="submit" name="loan_uid" value="<?php echo $orderDetails[$cntAA]->getLoanUid();?>">Print Invoice
                                      <input type="hidden" name="claim_status" value="4">
                                      <input type="hidden" name="invoice_no" value="<?php echo date('ymd',strtotime($orderDetails[$cntAA]->getRequestDate4()))."4".$orderDetails[$cntAA]->getId();?>">
                                      <input type="hidden" name="request_date" value="<?php echo $orderDetails[$cntAA]->getRequestDate4()?>">
                                          <!-- <img src="img/edit.png" class="edit-announcement-img hover1a" alt="Edit Product" title="Edit Product"> -->
                                          <!-- <img src="img/edit3.png" class="edit-announcement-img hover1b" alt="Edit Product" title="Edit Product"> -->
                                      </button></a>
                                  </form><?php
                                }else {
                                  if ($proDetails[0]->getProjectClaims() >= 4 ) {
                                  ?><form action="editInvoice.php" method="POST">
                                    <a><button class="clean edit-anc-btn hover1" style="color: blue" type="submit" name="loan_uid" value="<?php echo $orderDetails[$cntAA]->getLoanUid();?>">Issue Invoice
                                      <input type="hidden" name="claim_status" value="4">
                                      <input type="hidden" name="invoice_no" value="<?php echo date('ymd',strtotime($orderDetails[$cntAA]->getRequestDate4()))."4".$orderDetails[$cntAA]->getId();?>">
                                      <input type="hidden" name="request_date" value="<?php echo $orderDetails[$cntAA]->getRequestDate4()?>">
                                          <!-- <img src="img/edit.png" class="edit-announcement-img hover1a" alt="Edit Product" title="Edit Product"> -->
                                          <!-- <img src="img/edit3.png" class="edit-announcement-img hover1b" alt="Edit Product" title="Edit Product"> -->
                                      </button></a>
                                  </form><?php
                                }} ?><?php
                              } ?>
                            </tr>
                            <tr>
                              <?php if (!$orderDetails[$cntAA]->getCheckNo5() && $proDetails[0]->getProjectClaims() >= 5 && $orderDetails[$cntAA]->getClaimAmt5th()) {
                                ?>
                                <!-- <td class="td"><?php //echo $cntAA + 1?></td> -->
                                <td class="td"><?php echo $orderDetails[$cntAA]->getProjectName();?></td>
                                <td class="td"><?php echo $orderDetails[$cntAA]->getUnitNo();?></td>
                                <td class="td"><?php echo $orderDetails[$cntAA]->getRequestDate5();?></td>
                                <td class="td"><?php echo date('ymd',strtotime($orderDetails[$cntAA]->getRequestDate5()))."5".$orderDetails[$cntAA]->getId();?></td>
                                <td class="td">5th Commission</td>
                                <td class="td"><?php echo $orderDetails[$cntAA]->getClaimAmt5th();?></td>
                                <td class="td"><?php echo $orderDetails[$cntAA]->getSst5();?></td>
                                <?php if (!$orderDetails[$cntAA]->getCheckNo5() && $orderDetails[$cntAA]->getRequestDate5()) {
                                  ?><td class="td">PENDING</td><?php
                                }elseif (!$orderDetails[$cntAA]->getRequestDate5()) {
                                  ?><td class="td"></td><?php
                                }else {
                                  ?><td class="td">COMPLETED</td><?php
                                } ?>
                                <td class="td"><?php echo $orderDetails[$cntAA]->getReceiveDate5();?></td>
                                <td class="td">
                                  <?php if (!$orderDetails[$cntAA]->getCheckNo5()) {
                                ?><form class="" action="utilities/checkNo.php" method="post">
                                  <?php if ($orderDetails[$cntAA]->getCheckNo5()) {
                                    echo $orderDetails[$cntAA]->getCheckNo5();
                                  }else {
                                    ?>
                                    <input required class="no-outline" type="text" name="check_id" placeholder="CHECK NUMBER" value="<?php echo $orderDetails[$cntAA]->getCheckNo5();?>">
                                    <button class="button-add" type="submit" name="addCheckID5">Submit</button>
                                  <?php } ?>
                                  <!-- <input type="hidden" name="loan_uid" value="<?php //echo $orderDetails[$cntAA]->getLoanUid() ?>"> -->
                                  <input type="hidden" name="id" value="<?php echo $orderDetails[$cntAA]->getID() ?>">

                                  <button class="button-remove" type="submit" name="removeCheckID5">Delete</button>
                                </form>
                              <?php } ?>
                              </td>
                              <td class="td">
                                <?php if ($orderDetails[$cntAA]->getClaimAmt5th()) {
                                  ?><form action="invoice.php" method="POST">
                                    <a><button class="clean edit-anc-btn hover1" style="color: blue" type="submit" name="loan_uid" value="<?php echo $orderDetails[$cntAA]->getLoanUid();?>">Print Invoice
                                      <input type="hidden" name="claim_status" value="5">
                                      <input type="hidden" name="invoice_no" value="<?php echo date('ymd',strtotime($orderDetails[$cntAA]->getRequestDate5()))."5".$orderDetails[$cntAA]->getId();?>">
                                      <input type="hidden" name="request_date" value="<?php echo $orderDetails[$cntAA]->getRequestDate5()?>">
                                          <!-- <img src="img/edit.png" class="edit-announcement-img hover1a" alt="Edit Product" title="Edit Product"> -->
                                          <!-- <img src="img/edit3.png" class="edit-announcement-img hover1b" alt="Edit Product" title="Edit Product"> -->
                                      </button></a>
                                  </form><?php
                                }else {
                                  if ($proDetails[0]->getProjectClaims() >= 5 ) {
                                  ?><form action="editInvoice.php" method="POST">
                                    <a><button class="clean edit-anc-btn hover1" style="color: blue" type="submit" name="loan_uid" value="<?php echo $orderDetails[$cntAA]->getLoanUid();?>">Issue Invoice
                                      <input type="hidden" name="claim_status" value="5">
                                      <input type="hidden" name="invoice_no" value="<?php echo date('ymd',strtotime($orderDetails[$cntAA]->getRequestDate5()))."5".$orderDetails[$cntAA]->getId();?>">
                                      <input type="hidden" name="request_date" value="<?php echo $orderDetails[$cntAA]->getRequestDate5()?>">
                                          <!-- <img src="img/edit.png" class="edit-announcement-img hover1a" alt="Edit Product" title="Edit Product"> -->
                                          <!-- <img src="img/edit3.png" class="edit-announcement-img hover1b" alt="Edit Product" title="Edit Product"> -->
                                      </button></a>
                                  </form><?php
                                }} ?><?php
                              } ?>
                            </tr>
                            <?php
                            }
                        }
                    // }
                    // for($cnt = 0;$cnt < count($productsOrders) ;$cnt++)
                    // {
                        // $orderDetails = getLoanStatus($conn, $projectName);

                    //}
                    ?>
                </tbody>
            </table><br>


    </div>

    <!-- <div class="three-btn-container">
    <!-- <a href="adminRestoreProduct.php" class="add-a"><button name="restore_product" class="confirm-btn text-center white-text clean black-button anc-ow-btn two-button-side two-button-side1">Restore</button></a> -->
      <!-- <a href="adminAddNewProduct.php" class="add-a"><button name="add_new_product" class="confirm-btn text-center white-text clean black-button anc-ow-btn two-button-side  two-button-side2">Add</button></a> -->
    <!-- </div> -->
    <?php $conn->close();?>

</div>




<?php require_once dirname(__FILE__) . '/footer.php'; ?>
<?php include 'jsAdmin.php'; ?>

<?php
if(isset($_GET['type']))
{
    $messageType = null;

    if($_SESSION['messageType'] == 1)
    {
        if($_GET['type'] == 1)
        {
            $messageType = "Server currently fail. Please try again later.";
        }
        else if($_GET['type'] == 2)
        {
            $messageType = "Successfully Delete Product.";
        }
        else if($_GET['type'] == 3)
        {
            $messageType = "Error Deleting Product";
        }
        echo '
        <script>
            putNoticeJavascript("Notice !! ","'.$messageType.'");
        </script>
        ';
        $_SESSION['messageType'] = 0;
    }
}
?>
<script>
$(function () {
    $('.link-to-details').click(function () {
        window.location.href = $(this).data('url');
    });
})

</script>
<style>
      body {

      }
      input {

      font-weight: bold;
      /* color: white; */
      text-align: center;
      border-radius: 15px;
      }
      .no-outline:focus {
      outline: none;

      }
      button{
        border-radius: 15px;
        color: white;

      }
      .button-add{
        background-color: green;
      }
      .button-remove{
        background-color: red;
      }
    </style>
</body>
</html>
