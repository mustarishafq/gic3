<?php

require_once dirname(__FILE__) . '/1dbCon/dbCon.php';
require_once dirname(__FILE__) . '/adminAccess2.php';
require_once dirname(__FILE__) . '/sessionLoginChecker.php';

require_once dirname(__FILE__) . '/classes/Invoice.php';
require_once dirname(__FILE__) . '/classes/Commission.php';
require_once dirname(__FILE__) . '/classes/AdvancedSlip.php';
require_once dirname(__FILE__) . '/classes/LoanStatus.php';
require_once dirname(__FILE__) . '/classes/Project.php';

require_once dirname(__FILE__) . '/utilities/allNoticeModals.php';
require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';
// require_once dirname(__FILE__) . '/utilities/languageFunction.php';

$conn = connDB();

$advanceDetails = getAdvancedSlip($conn, "WHERE status = 'PENDING'");
$commissionDetails = getCommission($conn, "WHERE receive_status = 'PENDING'");
$loanDetails = getLoanStatus($conn);
$claim1st = 0;
$claim2nd = 0;
$claim3rd = 0;
$claim4th = 0;
$claim5th = 0;
// $invoiceDetails = getInvoice($conn);
 if ($loanDetails) {
  for ($cnt=0; $cnt <count($loanDetails) ; $cnt++) {
    $proDetails = getProject($conn, "WHERE project_name=?",array("project_name"),array($loanDetails[$cnt]->getProjectName()), "s");
    if ($proDetails) {
      if (!$loanDetails[$cnt]->getCheckNo1() && $proDetails[0]->getProjectClaims() >= 1 && $loanDetails[$cnt]->getClaimAmt1st()) {
        $claim1st += 1;
      }
      if (!$loanDetails[$cnt]->getCheckNo2() && $proDetails[0]->getProjectClaims() >= 2 && $loanDetails[$cnt]->getClaimAmt2nd()) {
        $claim2nd += 1;
      }
      if (!$loanDetails[$cnt]->getCheckNo3() && $proDetails[0]->getProjectClaims() >= 3 && $loanDetails[$cnt]->getClaimAmt3rd()) {
        $claim3rd += 1;
      }
      if (!$loanDetails[$cnt]->getCheckNo4() && $proDetails[0]->getProjectClaims() >= 4 && $loanDetails[$cnt]->getClaimAmt4th()) {
        $claim4th += 1;
      }
      if (!$loanDetails[$cnt]->getCheckNo5() && $proDetails[0]->getProjectClaims() >= 5 && $loanDetails[$cnt]->getClaimAmt5th()) {
        $claim5th += 1;
      }
    }

  }
  $totalInvoice = $claim1st + $claim2nd + $claim3rd + $claim4th + $claim5th;
}

// $conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <?php require_once dirname(__FILE__) . '/header.php'; ?>
	<?php include 'meta.php'; ?>
    <!--<meta property="og:url" content="https://dcksupreme.asia/" />-->
    <meta property="og:title" content="Dashboard | GIC" />
    <title>Dashboard | GIC</title>
    <!--<link rel="canonical" href="https://dcksupreme.asia/" />-->
    <?php include 'css.php'; ?>
</head>
<body class="body">
<?php //include 'header-admin.php'; ?>
<?php  include 'admin2Header.php'; ?>

<?php echo '<script type="text/javascript" src="js/jquery-3.3.1.min.js"></script>'; ?>

<div class="yellow-body same-padding">
	<h1 class="h1-title h1-before-border shipping-h1">Dashboard</h1>
    <div class="short-red-border"></div>
	<div class="width100 overflow section-divider">
        <a href="adminAddNewProject.php">
            <div class="five-red-btn-div">
                <p class="short-p five-red-p g-first-3-p">NEW PROJECT</p>
            </div>
        </a>
        <a href="editInvoiceGeneral.php">
            <div class="five-red-btn-div left-mid-red">
                <p class="short-p five-red-p f-first-3-p">ISSUE INVOICE</p>
            </div>
        </a>
        <a href="adminAdvanced.php">
            <div class="five-red-btn-div">
                <p class="short-p five-red-p e-first-3-p">ISSUE PAYROLL</p>
            </div>
        </a>

        <a href="adminCommission.php">
            <div class="five-red-btn-div right-mid-red">
                <p class="short-p issue-adv-p five-red-p">ISSUE COMMISSION</p>
            </div>
        </a>
        <a href="invoiceRecord2.php">
            <div class="five-red-btn-div">
                <p class="five-red-p">INVOICE RECORD</p>
            </div>
        </a>

    </div>
    <div class="clear"></div>
  <div class="width100">
    <?php if ($advanceDetails) {
      ?>
          <div class="red-dot"><p class="red-dot-p"><?php echo count($advanceDetails); ?></p></div><?php
        } ?>
  <div class="big-rectangle" id="white-big-box">
        <div class="left-side-title">
              <a style="color: black" href="adminAdvanced.php"> <h3 class="rec-h3">Advance</h3></a>
              <div class="short-red-border shorter"></div>
          </div>
          <?php if ($advanceDetails) {
          ?>
          <div class="right-side-title">
            <button class="clean show-all-btn red-link advance-a"  onclick="changeClass()">Show All</button>
          </div>
          <div class="clear"></div>
          <!-- repeat this div -->
          <?php for ($cnt=0; $cnt <count($advanceDetails) ; $cnt++) {
              ?><a href="adminAdvanced.php">
                  <div class="detailss-p red-color-hover">
                      <p class="small-date-p"><?php echo date('d/m/Y h:m a', strtotime( $advanceDetails[$cnt]->getDateCreated())); ?></p>
                      <p class="contents-p">
                          <?php echo "Unit No. ".$advanceDetails[$cnt]->getUnitNo() ?> Case Status Completed. Issue advance now.
                      </p>
                  </div>
              </a><?php
            }} ?>
      </div>
</div>

<div class="width100">
  <?php if ($commissionDetails) {
    ?>
    	<div class="red-dot"><p class="red-dot-p"><?php echo count($commissionDetails) ?></p></div>
    <?php } ?>
    	<div class="big-rectangle" id="white-big-boxC">
        	<div class="left-side-title">
                <a style="color: black" href="adminCommission.php"> <h3 class="rec-h3">Commission</h3></a>
                <div class="short-red-border shorter"></div>
            </div>
            <?php if ($commissionDetails) {
              ?>
            <div class="right-side-title">
            	<button class="clean show-all-btn red-link advance-a2"  onclick="changeClassC()">Show All</button>
            </div>
            <div class="clear"></div>
            <!-- repeat this div -->
            <?php for ($cnt=0; $cnt <count($commissionDetails) ; $cnt++) {
              ?><a href="adminCommission.php">
                  <div class="detailss-p red-color-hover">
                      <p class="small-date-p"><?php echo date('d/m/Y h:m a', strtotime($commissionDetails[$cnt]->getDateCreated())) ?></p>
                      <p class="contents-p">
                          Reminder: Commission of <?php echo $commissionDetails[$cnt]->getUpline() ?> shall be give out now.
                      </p>
                  </div>
              </a><?php
            }} ?>

            <!-- end of repeat this div -->
        </div>
    </div>

	<div class="width100">
    <?php $totalInvoice = $claim1st + $claim2nd + $claim3rd + $claim4th + $claim5th; ?>
    <?php if ($totalInvoice != 0) {
      ?><div class="red-dot"><p class="red-dot-p"><?php echo $totalInvoice ?></p></div><?php
    } ?>

    	<div class="big-rectangle" id="white-big-boxI">
        	<div class="left-side-title">
                <a href="invoiceFollowUp.php"><h3 style="color: black" class="rec-h3">Invoice Follow-up</h3> </a>
                <div class="short-red-border shorter"></div>
            </div>
            <?php if ($totalInvoice != 0) {
            ?><div class="right-side-title">
            	<button class="clean show-all-btn red-link advance-a4"  onclick="changeClassI()">Show All</button>
            </div><?php
          }?>
            <div class="clear"></div>
            <!-- repeat this div -->
            <?php
             if ($loanDetails) {
              for ($cnt=0; $cnt <count($loanDetails) ; $cnt++) {
                $proDetails = getProject($conn, "WHERE project_name=?",array("project_name"),array($loanDetails[$cnt]->getProjectName()), "s");
                if (!$loanDetails[$cnt]->getCheckNo1() && $proDetails[0]->getProjectClaims() >= 1 && $loanDetails[$cnt]->getClaimAmt1st()) {
                  $claim1st += 1;
                  ?><a href="invoiceFollowUp.php">
                      <div class="detailss-p red-color-hover">
                          <p class="small-date-p"><?php echo date('d/m/Y h:m a', strtotime($loanDetails[$cnt]->getRequestDate1())) ?></p>
                          <p class="contents-p">
                              Reminder: Follow-up Invoice No.<?php echo date('ymd', strtotime($loanDetails[$cnt]->getRequestDate1()))."1".$loanDetails[$cnt]->getId() ?>
                          </p>
                      </div>
                  </a><?php
                }
                if (!$loanDetails[$cnt]->getCheckNo2() && $proDetails[0]->getProjectClaims() >= 2 && $loanDetails[$cnt]->getClaimAmt2nd()) {
                  $claim2nd += 1;
                  ?><a href="invoiceFollowUp.php">
                      <div class="detailss-p red-color-hover">
                        <p class="small-date-p"><?php echo date('d/m/Y h:m a', strtotime($loanDetails[$cnt]->getRequestDate2())) ?></p>
                        <p class="contents-p">
                            Reminder: Follow-up Invoice No.<?php echo date('ymd', strtotime($loanDetails[$cnt]->getRequestDate2()))."2".$loanDetails[$cnt]->getId() ?>
                        </p>
                      </div>
                  </a><?php
                }
                if (!$loanDetails[$cnt]->getCheckNo3() && $proDetails[0]->getProjectClaims() >= 3 && $loanDetails[$cnt]->getClaimAmt3rd()) {
                  $claim3rd += 1;
                  ?><a href="invoiceFollowUp.php">
                      <div class="detailss-p red-color-hover">
                        <p class="small-date-p"><?php echo date('d/m/Y h:m a', strtotime($loanDetails[$cnt]->getRequestDate3())) ?></p>
                        <p class="contents-p">
                            Reminder: Follow-up Invoice No.<?php echo date('ymd', strtotime($loanDetails[$cnt]->getRequestDate3()))."3".$loanDetails[$cnt]->getId() ?>
                        </p>
                      </div>
                  </a><?php
                }
                if (!$loanDetails[$cnt]->getCheckNo4() && $proDetails[0]->getProjectClaims() >= 4 && $loanDetails[$cnt]->getClaimAmt4th()) {
                  $claim4th += 1;
                  ?><a href="invoiceFollowUp.php">
                      <div class="detailss-p red-color-hover">
                        <p class="small-date-p"><?php echo date('d/m/Y h:m a', strtotime($loanDetails[$cnt]->getRequestDate4())) ?></p>
                        <p class="contents-p">
                            Reminder: Follow-up Invoice No.<?php echo date('ymd', strtotime($loanDetails[$cnt]->getRequestDate4()))."4".$loanDetails[$cnt]->getId() ?>
                        </p>
                      </div>
                  </a><?php
                }
                if (!$loanDetails[$cnt]->getCheckNo5() && $proDetails[0]->getProjectClaims() >= 5 && $loanDetails[$cnt]->getClaimAmt5th()) {
                  $claim5th += 1;
                  ?><a href="invoiceFollowUp.php">
                      <div class="detailss-p red-color-hover">
                        <p class="small-date-p"><?php echo date('d/m/Y h:m a', strtotime($loanDetails[$cnt]->getRequestDate5())) ?></p>
                        <p class="contents-p">
                            Reminder: Follow-up Invoice No.<?php echo date('ymd', strtotime($loanDetails[$cnt]->getRequestDate5()))."5".$loanDetails[$cnt]->getId() ?>
                        </p>
                      </div>
                  </a><?php
                }
              }

            } ?>

            <!-- end of repeat this div -->
        </div>

    </div>


</div>
<?php require_once dirname(__FILE__) . '/footer.php'; ?>
<?php include 'jsAdmin.php'; ?>

<?php
if(isset($_GET['type']))
{
    $messageType = null;

    if($_SESSION['messageType'] == 1)
    {
        if($_GET['type'] == 1)
        {
            $messageType = "Server currently fail. Please try again later.";
        }
        else if($_GET['type'] == 2)
        {
            $messageType = "Successfully Delete Product.";
        }
        else if($_GET['type'] == 3)
        {
            $messageType = "Error Deleting Product";
        }
        echo '
        <script>
            putNoticeJavascript("Notice !! ","'.$messageType.'");
        </script>
        ';
        $_SESSION['messageType'] = 0;
    }
}
?>
<script>
$(function () {
    $('.link-to-details').click(function () {
        window.location.href = $(this).data('url');
    });
})

</script>

<script>
function changeClass() {
   var element = document.getElementById("white-big-box");
   element.classList.toggle("show-height");
}
</script>
<script>
		$(function(){
		   $(".advance-a").click(function () {
			  $(this).text(function(i, text){
				  return text === "Show All" ? "Hide" : "Show All";
			  })
		   });
		})
</script>
<script>
function changeClassC() {
   var element = document.getElementById("white-big-boxC");
   element.classList.toggle("show-height");
}
</script>
<script>
		$(function(){
		   $(".advance-a2").click(function () {
			  $(this).text(function(i, text){
				  return text === "Show All" ? "Hide" : "Show All";
			  })
		   });
		})
</script>
<script>
function changeClassL() {
   var element = document.getElementById("white-big-boxL");
   element.classList.toggle("show-height");
}
</script>
<script>
		$(function(){
		   $(".advance-a3").click(function () {
			  $(this).text(function(i, text){
				  return text === "Show All" ? "Hide" : "Show All";
			  })
		   });
		})
</script>
<script>
function changeClassI() {
   var element = document.getElementById("white-big-boxI");
   element.classList.toggle("show-height");
}
</script>
<script>
		$(function(){
		   $(".advance-a4").click(function () {
			  $(this).text(function(i, text){
				  return text === "Show All" ? "Hide" : "Show All";
			  })
		   });
		})
</script>
</body>
</html>
