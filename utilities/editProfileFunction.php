<?php
require_once dirname(__FILE__) . '/../1dbCon/dbCon.php';
require_once dirname(__FILE__) . '/../sessionLoginChecker.php';

require_once dirname(__FILE__) . '/../classes/User.php';

require_once dirname(__FILE__) . '/allNoticeModals.php';
require_once dirname(__FILE__) . '/databaseFunction.php';
require_once dirname(__FILE__) . '/generalFunction.php';

    if($_SERVER['REQUEST_METHOD'] == 'POST')
    {
        $conn = connDB();

        $uid = $_SESSION['uid'];

        $fullname = rewrite($_POST["update_fullname"]);
        // $username = rewrite($_POST["update_username"]);
        $icNo = rewrite($_POST["update_icno"]);
        $phoneNo = rewrite($_POST["update_phoneno"]);
        $address = rewrite($_POST["update_address"]);
        $update_email = rewrite($_POST['update_email']);
        $birthday = rewrite($_POST["update_birthday"]);
        $bankName = rewrite($_POST["update_bankname"]);
        $bankAccountNo = rewrite($_POST["update_bankaccountnumber"]);

        //   FOR DEBUGGING 
        // echo "<br>";
        // echo $fullname."<br>";
        // echo $register_email."<br>";
        // echo $register_contact."<br>";

        $user = getUser($conn," id = ?   ",array("id"),array($uid),"s");    

        if(!$user)
        {   
            $tableName = array();
            $tableValue =  array();
            $stringType =  "";
            //echo "save to database";

            if($fullname)
            {
                array_push($tableName,"full_name");
                array_push($tableValue,$fullname);
                $stringType .=  "s";
            }
            // if($username)
            // {
            //     array_push($tableName,"username");
            //     array_push($tableValue,$username);
            //     $stringType .=  "s";
            // }
            if($icNo)
            {
                array_push($tableName,"ic");
                array_push($tableValue,$icNo);
                $stringType .=  "s";
            }
            if($phoneNo)
            {
                array_push($tableName,"contact");
                array_push($tableValue,$phoneNo);
                $stringType .=  "s";
            }
            if($address)
            {
                array_push($tableName,"address");
                array_push($tableValue,$address );
                $stringType .=  "s";
            }
            if($update_email)
            {
                array_push($tableName,"email");
                array_push($tableValue,$update_email);
                $stringType .=  "s";
            }
            if($birthday)
            {
                array_push($tableName,"birth_month");
                array_push($tableValue,$birthday);
                $stringType .=  "s";
            }
            if($bankName)
            {
                array_push($tableName,"bank");
                array_push($tableValue,$bankName );
                $stringType .=  "s";
            }
            if($bankAccountNo)
            {
                array_push($tableName,"bank_no");
                array_push($tableValue,$bankAccountNo );
                $stringType .=  "s";
            }

            array_push($tableValue,$uid);
            $stringType .=  "s";
            $passwordUpdated = updateDynamicData($conn,"user"," WHERE id = ? ",$tableName,$tableValue,$stringType);
            if($passwordUpdated)
            {
                // echo "success";
                $_SESSION['messageType'] = 1;
                header('Location: ../editProfile.php?type=1');
            }
            else
            {
                // echo "fail";
                $_SESSION['messageType'] = 1;
                header('Location: ../editProfile.php?type=2');
            }
        }
        else
        {
            //echo "";
            $_SESSION['messageType'] = 1;
            header('Location: ../editProfile.php?type=1');
        }

    }
else 
{
    //header('Location: ../editprofile.php');
    header('Location: ../index.php');
}
?>
