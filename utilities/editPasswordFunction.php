<?php
require_once dirname(__FILE__) . '/../1dbCon/dbCon.php';
require_once dirname(__FILE__) . '/../sessionLoginChecker.php';

require_once dirname(__FILE__) . '/../classes/User.php';

// require_once dirname(__FILE__) . '/allNoticeModals.php';
require_once dirname(__FILE__) . '/databaseFunction.php';
require_once dirname(__FILE__) . '/generalFunction.php';
// require_once dirname(__FILE__) . '/mailerFunction.php';


if($_SERVER['REQUEST_METHOD'] == 'POST')
{
     $conn = connDB();

     $username  = $_SESSION['username'];

     $editPassword_current  = $_POST['editPassword_current'];
     $editPassword_new  = $_POST['editPassword_new'];
     $editPassword_reenter  = $_POST['editPassword_reenter'];

     $user = getUser($conn," WHERE username = ? ",array("username"),array($username),"s");
     $userDetails = $user[0];
     // echo $userDetails->getUsername();

     $dbPass =  $userDetails->getPassword();
     $dbSalt =  $userDetails->getSalt();

     $editPassword_current_hashed = hash('sha256',$editPassword_current);
     $editPassword_current_hashed_salted = hash('sha256', $dbSalt . $editPassword_current_hashed);

     // echo $dbPass."<br>";
     // echo $finalPassword."<br>";

     if($editPassword_current_hashed_salted == $dbPass)
     {
          if(strlen($editPassword_new) >= 6 && strlen($editPassword_reenter) >= 6 )
          {
               if($editPassword_new == $editPassword_reenter)
               {
                    $password = hash('sha256',$editPassword_new);
                    $salt = substr(sha1(mt_rand()), 0, 100);
                    $finalPassword = hash('sha256', $salt.$password);

                    $passwordUpdated = updateDynamicData($conn,"user"," WHERE username = ? ",array("password","salt"),array($finalPassword,$salt,$username),"sss");
                    if($passwordUpdated)
                    {
                         $_SESSION['messageType'] = 1;
                         header( "Location: ../editPassword.php?type=5" );
                         //echo "//Update Password success ";
                    }
                    else 
                    {
                         $_SESSION['messageType'] = 1;
                         header( "Location: ../editPassword.php?type=4" );
                         //echo "//server problem ";
                    }
               }
               else 
               {
                    $_SESSION['messageType'] = 1;
                    header( "Location: ../editPassword.php?type=3" );
                    //echo "//password must be same with reenter ";
               }
          }
          else 
          {
               $_SESSION['messageType'] = 1;
               header( "Location: ../editPassword.php?type=2" );
               //echo "//password length must be more than 6 ";
          }
     }
     else 
     {
          $_SESSION['messageType'] = 1;
          header( "Location: ../editPassword.php?type=1" );
          //echo "//Current Password is not the same as previous ";
     }    
}
?>