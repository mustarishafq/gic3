<?php
require_once dirname(__FILE__) . '/../sessionLoginChecker.php';
require_once dirname(__FILE__) . '/../1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/../classes/User.php';
require_once dirname(__FILE__) . '/../classes/Project.php';

require_once dirname(__FILE__) . '/databaseFunction.php';
require_once dirname(__FILE__) . '/generalFunction.php';

function addNewProject($conn,$projectName,$addProjectPpl,$claimTimes,$projectLeader)
{
     if(insertDynamicData($conn,"project",array("project_name","add_projectppl","claims_no","project_leader"),
     array($projectName,$addProjectPpl,$claimTimes,$projectLeader),"ssss") === null)
     {
          //    echo $finalPassword;
     }
     else
     {
          //   echo "bbbb";
     }
     return true;
}

if($_SERVER['REQUEST_METHOD'] == 'POST')
{
     $conn = connDB();

//     $id = md5(uniqid());
    $projectName = rewrite($_POST["project_name"]);
    $addProjectPpl = rewrite($_POST["add_by"]);
    $claimTimes = rewrite($_POST["claims_times"]);
    $projectLeader = rewrite($_POST["project_leader"]);

     //   FOR DEBUGGING
     //    echo $name;
     //    echo $price;
     //    echo $type;

     if(addNewProject($conn,$projectName,$addProjectPpl,$claimTimes,$projectLeader))
     {
          $_SESSION['messageType'] = 1;
          header('Location: ../admin1Product.php?type=4');
          // echo "register success";
          // echo "<script>alert('New Project Created Successfully !');window.location='../admin1Product.php'</script>";
     }

}
else
{
    //  header('Location: ../index.php');
}
?>
