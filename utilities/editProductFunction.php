<?php
require_once dirname(__FILE__) . '/../sessionLoginChecker.php';
require_once dirname(__FILE__) . '/../1dbCon/dbCon.php';
require_once dirname(__FILE__) . '/../timezone.php';

require_once dirname(__FILE__) . '/../classes/User.php';
require_once dirname(__FILE__) . '/../classes/Commission.php';
require_once dirname(__FILE__) . '/../classes/Product2.php';
require_once dirname(__FILE__) . '/../classes/LoanStatus.php';
require_once dirname(__FILE__) . '/../classes/AdvancedSlip.php';

require_once dirname(__FILE__) . '/databaseFunction.php';
require_once dirname(__FILE__) . '/generalFunction.php';

$conn = connDB();


function addAdvancedSlip($conn, $idd, $unitNo, $projectName, $bookingDate, $loanUid, $agent, $amount, $status, $receiveStatus)
{
     if(insertDynamicData($conn,"advance_slip", array("id", "unit_no", "project_name", "booking_date", "loan_uid","agent","amount","status", "receive_status"),
     array($idd, $unitNo,$projectName, $bookingDate, $loanUid, $agent, $amount, $status, $receiveStatus),
     "isssssdss") === null)
     {
          //    echo $finalPassword;
     }
     else
     {
          //   echo "bbbb";
     }

     return true;
}
//

if($_SERVER['REQUEST_METHOD'] == 'POST')
{
     $conn = connDB();

    $id = rewrite($_POST["id"]);
    $unitNo = $_POST["unit_no"];
    $unitNoImplode = implode(",",$unitNo);
    $loanUid = rewrite($_POST["loan_uid"]);
    $loanDetails = getLoanStatus($conn, "WHERE loan_uid = ?", array("loan_uid"), array($loanUid), "s");
    $currentAgentComm = $loanDetails[0]->getAgentComm();

    $advanceDetails = getAdvancedSlip($conn, "WHERE loan_uid = ? ", array("loan_uid"), array($loanUid), "s");

    $amount = 2000;
    $status = 'PENDING';
    $receiveStatus = 'PENDING';
    $purchaserName = $_POST["purchaser_name"];
    $purchaserNameImplode = implode(",",$purchaserName);
    $ic = $_POST["ic"];
    $icImplode = implode(",",$ic);
    $contact = $_POST["contact"];
    $contactImplode = implode(",",$contact);
    $email = $_POST["email"];
    $emailImplode = implode(",",$email);
    $bookingDate = rewrite($_POST["booking_date"]);
    $sqFt = rewrite($_POST["sq_ft"]);

    // $spaPrice = rewrite($_POST["spa_price"]);
    $str2 = rewrite($_POST["spa_price"]);
    $newSPAPrice = str_replace( ',', '', $str2);
    $spaPrice = $newSPAPrice;

    $package = rewrite($_POST["package"]);
    $discount = rewrite($_POST["discount"]);
    $rebate = rewrite($_POST["rebate"]);
    $extraRebate = rewrite($_POST["extra_rebate"]);

    //remove comma inside value
    // $nettPrice = rewrite($_POST["nettprice"]);

    $str1 = rewrite($_POST["nettprice"]);
    $newNettPrice = str_replace( ',', '', $str1);
    $nettPrice = $newNettPrice;

    // $totalDeveloperComm = rewrite($_POST["totaldevelopercomm"]);

    //choose % or value
    $totalDC = rewrite($_POST["totaldevelopercomm"]);
    if (!$totalDC)
    {
         $totalDCP = rewrite($_POST["totaldevelopercommper"]);
         $tDCP = ($totalDCP / 100);
         $totalDeveloperComm = ($tDCP * $nettPrice);

         // $newTotalDCP = str_replace( '%', '', $totalDCP);
         // $nettPrice = $newNettPrice;
         // $totalDeveloperComm = $totalDCP;
    }
    else
    {
         $totalDCstr = rewrite($_POST["totaldevelopercomm"]);

         $totalDeveloperComm = $totalDCstr;
         // $newTotalDCstr = str_replace( ',', '', $totalDCstr);
         // $totalDeveloperComm = $newTotalDCstr;
    }

    //get upline, up-upline
    $agent = rewrite($_POST["agent"]);
    $getUplineDetails = getUser($conn," WHERE username = ? ",array("username"),array($agent),"s");
    $upline1Name = $getUplineDetails[0]->getUpline1();
    $upline2Name = $getUplineDetails[0]->getUpline2();

    $loanStatus = rewrite($_POST["loanstatus"]);
    $remark = rewrite($_POST["remark"]);
    $bFormCollected = rewrite($_POST["bform_Collected"]);
    $paymentMethod = rewrite($_POST["payment_method"]);
    $lawyer = rewrite($_POST["lawyer"]);
    $pendingApprovalStatus = rewrite($_POST["pending_approval_status"]);
    $bankApproved = rewrite($_POST["bank_approved"]);
    $loSignedDate = rewrite($_POST["lo_signed_date"]);
    $laSignedDate = rewrite($_POST["la_signed_date"]);
    $spaSignedDate = rewrite($_POST["spa_signed_date"]);
    $fullsetCompleted = rewrite($_POST["fullset_completed"]);

    $cashBuyer = rewrite($_POST["cash_buyer"]);

    $cancelledBooking = rewrite($_POST["cancelled_booking"]);
    $caseStatus = rewrite($_POST["case_status"]);
    $projectName = rewrite($_POST['project_name']);

    // if ($caseStatus == 'COMPLETED') {
    //   $upline = rewrite($_POST['upline']);
    //   $upUpline = rewrite($_POST['up_upline']);
    //   $uplineCommission = rewrite($_POST['upline_commission']);
    //   $uulCommission = rewrite($_POST['up_upline_commission']);
    // }

    $eventPersonal = rewrite($_POST["event_personal"]);

    $upline1 = $upline1Name;
    if (!$upline1)
    {
        $upline1 = "null";
    }
    $upline2 = $upline2Name;
    if (!$upline2)
    {
        $upline2 = "null";
    }

    $plName = rewrite($_POST["pl_name"]);
    $hosName = rewrite($_POST["hos_name"]);
    $listerName = rewrite($_POST["lister_name"]);


    $rate = rewrite($_POST["rate"]);
    if (!$rate)
    {
        $rate = "";
        // $rate = rewrite($_POST["rate"]);
        $agentComm = rewrite($_POST["agent_comm"]);

        // $ulOverride = rewrite($_POST["ul_override"]);
        // $uulOverride = rewrite($_POST["uul_override"]);
        // $plOverride = rewrite($_POST["pl_override"]);
        // $hosOverride = rewrite($_POST["hos_override"]);

        $ulOverride = $agentComm * (5/100);
        $uulOverride = $agentComm * (5/100);
        $plOverride = $agentComm * (15/100);
        $hosOverride = $agentComm * (5/100);

        $noRate = $agentComm + $ulOverride + $uulOverride + $plOverride + $hosOverride;

        $listerOverride = rewrite($_POST["lister_override"]);

        $admin1Override = rewrite($_POST["admin1_override"]);
        $admin2Override = rewrite($_POST["admin2_override"]);
        $admin3Override = rewrite($_POST["admin3_override"]);

        $totalAdminOverride = $listerOverride + $admin1Override + $admin2Override + $admin3Override;
        $gicProfit = $totalDeveloperComm - $noRate - $totalAdminOverride;

    }
    else
    {
        $ratePercentage =( $rate / 100 );
        $agentComm = $ratePercentage * $nettPrice;
        $ulOverride = $agentComm * (5/100);
        $uulOverride = $agentComm * (5/100);
        $plOverride = $agentComm * (15/100);
        $hosOverride = $agentComm * (5/100);

        $withRate = $agentComm + $ulOverride + $uulOverride + $plOverride + $hosOverride;

        $listerOverride = rewrite($_POST["lister_override"]);

        $admin1Override = rewrite($_POST["admin1_override"]);
        $admin2Override = rewrite($_POST["admin2_override"]);
        $admin3Override = rewrite($_POST["admin3_override"]);

        $totalAdminOverride = $listerOverride + $admin1Override + $admin2Override + $admin3Override;
        $gicProfit = $totalDeveloperComm - $withRate - $totalAdminOverride;
    }

    $totalClaimedDevAmt = $loanDetails[0]->getTotalClaimDevAmt();
    $totalBalUnclaimAmt = $loanDetails[0]->getTotalBalUnclaimAmt();

    $commissionDetails = getCommission($conn,"WHERE loan_uid = ? ", array("loan_uid"), array($loanUid), "s");

}

if(isset($_POST['editSubmit']))
{
    $tableName = array();
    $tableValue =  array();
    $stringType =  "";
    // //echo "save to database";
    if($purchaserNameImplode)
    {
        array_push($tableName,"purchaser_name");
        array_push($tableValue,$purchaserNameImplode);
        $stringType .=  "s";
    }
    if($unitNoImplode)
    {
        array_push($tableName,"unit_no");
        array_push($tableValue,$unitNoImplode);
        $stringType .=  "s";
    }if($icImplode)
    {
        array_push($tableName,"ic");
        array_push($tableValue,$icImplode);
        $stringType .=  "s";
    }if($contactImplode)
    {
        array_push($tableName,"contact");
        array_push($tableValue,$contactImplode);
        $stringType .=  "s";
    }
    if($emailImplode)
    {
        array_push($tableName,"email");
        array_push($tableValue,$emailImplode);
        $stringType .=  "s";
    }
    if($bookingDate)
    {
        array_push($tableName,"booking_date");
        array_push($tableValue,$bookingDate);
        $stringType .=  "s";
    }
    if($sqFt)
    {
        array_push($tableName,"sq_ft");
        array_push($tableValue,$sqFt);
        $stringType .=  "s";
    }
    if($spaPrice)
    {
        array_push($tableName,"spa_price");
        array_push($tableValue,$spaPrice);
        $stringType .=  "s";
    }
    if($package)
    {
        array_push($tableName,"package");
        array_push($tableValue,$package);
        $stringType .=  "s";
    }
    if($discount)
    {
        array_push($tableName,"discount");
        array_push($tableValue,$discount);
        $stringType .=  "s";
    }if($rebate)
    {
        array_push($tableName,"rebate");
        array_push($tableValue,$rebate);
        $stringType .=  "s";
    }if($extraRebate)
    {
        array_push($tableName,"extra_rebate");
        array_push($tableValue,$extraRebate);
        $stringType .=  "s";
    }
    if($nettPrice)
    {
        array_push($tableName,"nettprice");
        array_push($tableValue,$nettPrice);
        $stringType .=  "s";
    }
    if($totalDeveloperComm)
    {
        array_push($tableName,"totaldevelopercomm");
        array_push($tableValue,$totalDeveloperComm);
        $stringType .=  "s";
    }
    if($agent)
    {
        array_push($tableName,"agent");
        array_push($tableValue,$agent);
        $stringType .=  "s";
    }
    if($loanStatus)
    {
        array_push($tableName,"loanstatus");
        array_push($tableValue,$loanStatus);
        $stringType .=  "s";
    }
    if($remark)
    {
        array_push($tableName,"remark");
        array_push($tableValue,$remark);
        $stringType .=  "s";
    }
    if($bFormCollected)
    {
        array_push($tableName,"bform_Collected");
        array_push($tableValue,$bFormCollected);
        $stringType .=  "s";
    }if($paymentMethod)
    {
        array_push($tableName,"payment_method");
        array_push($tableValue,$paymentMethod);
        $stringType .=  "s";
    }if($lawyer)
    {
        array_push($tableName,"lawyer");
        array_push($tableValue,$lawyer);
        $stringType .=  "s";
    }
    if($pendingApprovalStatus)
    {
        array_push($tableName,"pending_approval_status");
        array_push($tableValue,$pendingApprovalStatus);
        $stringType .=  "s";
    }
    if($bankApproved)
    {
        array_push($tableName,"bank_approved");
        array_push($tableValue,$bankApproved);
        $stringType .=  "s";
    }
    if($loSignedDate)
    {
        array_push($tableName,"lo_signed_date");
        array_push($tableValue,$loSignedDate);
        $stringType .=  "s";
    }
    if($laSignedDate)
    {
        array_push($tableName,"la_signed_date");
        array_push($tableValue,$laSignedDate);
        $stringType .=  "s";
    }
    if($spaSignedDate)
    {
        array_push($tableName,"spa_signed_date");
        array_push($tableValue,$spaSignedDate);
        $stringType .=  "s";
    }
    if($fullsetCompleted)
    {
        array_push($tableName,"fullset_completed");
        array_push($tableValue,$fullsetCompleted);
        $stringType .=  "s";
    }
    if($cashBuyer)
    {
        array_push($tableName,"cash_buyer");
        array_push($tableValue,$cashBuyer);
        $stringType .=  "s";
    }
    if($cancelledBooking)
    {
        array_push($tableName,"cancelled_booking");
        array_push($tableValue,$cancelledBooking);
        $stringType .=  "s";
    }
    if($caseStatus)
    {
        array_push($tableName,"case_status");
        array_push($tableValue,$caseStatus);
        $stringType .=  "s";
    }
    if($eventPersonal)
    {
        array_push($tableName,"event_personal");
        array_push($tableValue,$eventPersonal);
        $stringType .=  "s";
    }
    if($rate)
    {
        array_push($tableName,"rate");
        array_push($tableValue,$rate);
        $stringType .=  "s";
    }
    if($agentComm)
    {
        // $afterDeductAdv = $currentAgentComm - $amount;

        array_push($tableName,"agent_comm");
        array_push($tableValue,$agentComm);
        $stringType .=  "s";
    }
    if($upline1)
    {
        array_push($tableName,"upline1");
        array_push($tableValue,$upline1);
        $stringType .=  "s";
    }
    if($upline2)
    {
        array_push($tableName,"upline2");
        array_push($tableValue,$upline2);
        $stringType .=  "s";
    }
    if($plName)
    {
        array_push($tableName,"pl_name");
        array_push($tableValue,$plName);
        $stringType .=  "s";
    }if($hosName)
    {
        array_push($tableName,"hos_name");
        array_push($tableValue,$hosName);
        $stringType .=  "s";
    }if($listerName)
    {
        array_push($tableName,"lister_name");
        array_push($tableValue,$listerName);
        $stringType .=  "s";
    }
    if($ulOverride)
    {
        array_push($tableName,"ul_override");
        array_push($tableValue,$ulOverride);
        $stringType .=  "s";
    }
    if($uulOverride)
    {
        array_push($tableName,"uul_override");
        array_push($tableValue,$uulOverride);
        $stringType .=  "s";
    }
    if($plOverride)
    {
        array_push($tableName,"pl_override");
        array_push($tableValue,$plOverride);
        $stringType .=  "s";
    }
    if($hosOverride)
    {
        array_push($tableName,"hos_override");
        array_push($tableValue,$hosOverride);
        $stringType .=  "s";
    }
    if($listerOverride)
    {
        array_push($tableName,"lister_override");
        array_push($tableValue,$listerOverride);
        $stringType .=  "s";
    }
    if($admin1Override)
    {
        array_push($tableName,"admin1_override");
        array_push($tableValue,$admin1Override);
        $stringType .=  "s";
    }if($admin2Override)
    {
        array_push($tableName,"admin2_override");
        array_push($tableValue,$admin2Override);
        $stringType .=  "s";
    }
    if($admin3Override)
    {
        array_push($tableName,"admin3_override");
        array_push($tableValue,$admin3Override);
        $stringType .=  "s";
    }
    if($gicProfit)
    {
        array_push($tableName,"gic_profit");
        array_push($tableValue,$gicProfit);
        $stringType .=  "s";
    }
    if($totalClaimedDevAmt && !$advanceDetails)
    {
        // $totalClaimedDevAmt += $amount;
        array_push($tableName,"total_claimed_dev_amt");
        array_push($tableValue,$totalClaimedDevAmt);
        $stringType .=  "s";
    }
    if($totalClaimedDevAmt && $advanceDetails)
    {
        array_push($tableName,"total_claimed_dev_amt");
        array_push($tableValue,$totalClaimedDevAmt);
        $stringType .=  "s";
    }
    if(!$totalClaimedDevAmt && !$advanceDetails)
    {
        // $totalClaimedDevAmt += $amount;
        array_push($tableName,"total_claimed_dev_amt");
        array_push($tableValue,$totalClaimedDevAmt);
        $stringType .=  "s";
    }
    if(!$totalClaimedDevAmt && $advanceDetails)
    {
        array_push($tableName,"total_claimed_dev_amt");
        array_push($tableValue,$totalClaimedDevAmt);
        $stringType .=  "s";
    }
    if($totalBalUnclaimAmt && !$advanceDetails)
    {
        $totalBalUnclaimAmt -= $amount;
        array_push($tableName,"total_bal_unclaim_amt");
        array_push($tableValue,$totalBalUnclaimAmt);
        $stringType .=  "s";
    }
    if($totalBalUnclaimAmt && $advanceDetails)
    {
        array_push($tableName,"total_bal_unclaim_amt");
        array_push($tableValue,$totalBalUnclaimAmt);
        $stringType .=  "s";
    }
// if($name)
// {
//
//   move_uploaded_file($_FILES['file']['tmp_name'],$target_file);
//
//     array_push($tableName,"images");
//     array_push($tableValue,$name);
//     $stringType .=  "s";
// //}
// } // if error close this

    array_push($tableValue,$loanUid);
    $stringType .=  "s";
    $withdrawUpdated = updateDynamicData($conn,"loan_status"," WHERE loan_uid = ? ",$tableName,$tableValue,$stringType);

    if($withdrawUpdated)
    {
      if ($caseStatus == 'COMPLETED' && !$advanceDetails) {

       //  if(addUplineCommission($conn, $id, $purchaserName, $loanUid, $ulOverride,$upline1,$projectName,$unitNo,$bookingDate))
       //       {
       //            //$_SESSION['messageType'] = 1;
       //            // header('Location: ../admin1Product.php');
       //            //echo "register success";
       //       }
       //
       // if (addUpuplineCommission($conn, $id2, $purchaserName, $loanUid, $uulOverride,$upline2,$projectName,$unitNo,$bookingDate))
       // {
       //   header('Location: ../admin1Product.php');
       // }

       if(addAdvancedSlip($conn, $idd, $unitNo, $projectName, $bookingDate, $loanUid, $agent, $amount, $status, $receiveStatus))
            {
                 //$_SESSION['messageType'] = 1;
                 // header('Location: ../admin1Product.php');
                 //echo "register success";
            }
      }
        $_SESSION['messageType'] = 1;
        header('Location: ../admin1Product.php');



    }
    else
    {
        echo "fail";

    }
}
else
{
  //  echo "dunno";

}

  if( isset($_POST['deleteProduct']) )
  	{
  		$id = $_POST['id'];
  		$sql= "UPDATE product SET display=0 WHERE id=$id";

if ($conn->query($sql) === TRUE) {
  header('Location: ../adminProduct.php');
} else {
  header('Location: ../adminProduct.php');
}

  	}

    if( isset($_POST['restoreSubmit']) )
    	{
    		$id = $_POST['id'];
    		$sql= "UPDATE product SET display=1 WHERE id=$id";

  if ($conn->query($sql) === TRUE) {
    header('Location: ../adminProduct.php');
  } else {
    header('Location: ../adminProduct.php');
  }

    	}



?>
