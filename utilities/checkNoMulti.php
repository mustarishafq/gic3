<?php
require_once dirname(__FILE__) . '/../sessionLoginChecker.php';
// require_once dirname(__FILE__) . '/../adminAccess1.php';
require_once dirname(__FILE__) . '/../timezone.php';
require_once dirname(__FILE__) . '/../1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/../classes/User.php';
require_once dirname(__FILE__) . '/../classes/MultiInvoice.php';
require_once dirname(__FILE__) . '/../classes/LoanStatus.php';
require_once dirname(__FILE__) . '/../classes/Product2.php';
require_once dirname(__FILE__) . '/../classes/Project.php';

require_once dirname(__FILE__) . '/databaseFunction.php';
require_once dirname(__FILE__) . '/generalFunction.php';

$conn = connDB();

function addUplineCommission($conn, $idd, $purchaserName, $loanUid, $ulOverride,$upline1,$projectName,$unitNo,$bookingDate, $receiveStatus, $details)
{
     if(insertDynamicData($conn,"commission", array("id","purchaser_name","loan_uid","commission","upline","project_name","unit_no","booking_date", "receive_status","details"),
     array($idd, $purchaserName, $loanUid, $ulOverride,$upline1,$projectName,$unitNo,$bookingDate, $receiveStatus, $details),
     "issdssssss") === null)
     {
          //    echo $finalPassword;
     }
     else
     {
          //   echo "bbbb";
     }

     return true;
}
function addUpuplineCommission($conn, $idd2, $purchaserName, $loanUid, $uulOverride,$upline2,$projectName,$unitNo,$bookingDate, $receiveStatus, $details)
{
     if(insertDynamicData($conn,"commission", array("id","purchaser_name","loan_uid","commission","upline","project_name","unit_no","booking_date","receive_status","details"),
     array($idd2, $purchaserName, $loanUid, $uulOverride,$upline2,$projectName,$unitNo,$bookingDate, $receiveStatus, $details),
     "issdssssss") === null)
     {
          //    echo $finalPassword;
     }
     else
     {
          //   echo "bbbb";
     }

     return true;
}
function addAgentCommission($conn, $idd3, $purchaserName, $loanUid, $agentCommFinal,$agent,$projectName,$unitNo,$bookingDate, $receiveStatus, $details)
{
     if(insertDynamicData($conn,"commission", array("id","purchaser_name","loan_uid","commission","upline","project_name","unit_no","booking_date","receive_status","details"),
     array($idd3, $purchaserName, $loanUid, $agentCommFinal,$agent,$projectName,$unitNo,$bookingDate, $receiveStatus, $details),
     "issdssssss") === null)
     {
          //    echo $finalPassword;
     }
     else
     {
          //   echo "bbbb";
     }

     return true;
}
function addPlCommission($conn, $idd3, $purchaserName, $loanUid, $plOverrideFinal,$plName,$projectName,$unitNo,$bookingDate, $receiveStatus, $details)
{
     if(insertDynamicData($conn,"commission", array("id","purchaser_name","loan_uid","commission","upline","project_name","unit_no","booking_date","receive_status","details"),
     array($idd3, $purchaserName, $loanUid, $plOverrideFinal,$plName,$projectName,$unitNo,$bookingDate, $receiveStatus, $details),
     "issdssssss") === null)
     {
          //    echo $finalPassword;
     }
     else
     {
          //   echo "bbbb";
     }

     return true;
}
function addHosCommission($conn, $idd3, $purchaserName, $loanUid, $hosOverrideFinal,$hosName,$projectName,$unitNo,$bookingDate, $receiveStatus, $details)
{
     if(insertDynamicData($conn,"commission", array("id","purchaser_name","loan_uid","commission","upline","project_name","unit_no","booking_date","receive_status","details"),
     array($idd3, $purchaserName, $loanUid, $hosOverrideFinal,$hosName,$projectName,$unitNo,$bookingDate, $receiveStatus, $details),
     "issdssssss") === null)
     {
          //    echo $finalPassword;
     }
     else
     {
          //   echo "bbbb";
     }

     return true;
}
function addListerCommission($conn, $idd3, $purchaserName, $loanUid, $listerOverrideFinal,$listerName,$projectName,$unitNo,$bookingDate, $receiveStatus, $details)
{
     if(insertDynamicData($conn,"commission", array("id","purchaser_name","loan_uid","commission","upline","project_name","unit_no","booking_date","receive_status","details"),
     array($idd3, $purchaserName, $loanUid, $listerOverrideFinal,$listerName,$projectName,$unitNo,$bookingDate, $receiveStatus, $details),
     "issdssssss") === null)
     {
          //    echo $finalPassword;
     }
     else
     {
          //   echo "bbbb";
     }

     return true;
}
//========================================================================================================================================================
if (isset($_POST['removeCheckID'])) { // for remove button
  // $receiveStatus = 'PENDING';
  $checkID = null;
  $receiveDate = null;

  $tableName = array();
  $tableValue =  array();
  $stringType =  "";
  // //echo "save to database";
  // if(!$checkID1)
  // {
  //     array_push($tableName,"check_no1");
  //     array_push($tableValue,$checkID1);
  //     $stringType .=  "s";
  // }
  if(!$receiveDate)
  {
      array_push($tableName,"receive_date");
      array_push($tableValue,$receiveDate);
      $stringType .=  "s";
  }
  // if($totalClaimedDevAmtRestore || !$totalClaimedDevAmtRestore)
  // {
  //     array_push($tableName,"total_claimed_dev_amt");
  //     array_push($tableValue,$totalClaimedDevAmtRestore);
  //     $stringType .=  "s";
  // }
  // if($totalBalUnclaimAmtRestore || !$totalBalUnclaimAmtRestore)
  // {
  //     array_push($tableName,"total_bal_unclaim_amt");
  //     array_push($tableValue,$totalBalUnclaimAmtRestore);
  //     $stringType .=  "s";
  // }
  array_push($tableValue,$invoiceMultiId);
  $stringType .=  "s";
  $withdrawUpdated = updateDynamicData($conn,"invoice_multi"," WHERE id = ? ",$tableName,$tableValue,$stringType);

  if($withdrawUpdated)
  {
    header('Location: ../invoiceRecordMulti.php');
  }
  $tableName = array();
  $tableValue =  array();
  $stringType =  "";
     $stringType .=  "s";
  // }
  if(!$receiveDate)
  {
      array_push($tableName,"receive_date");
      array_push($tableValue,$receiveDate);
      $stringType .=  "s";
  }
  array_push($tableValue,$id);
  $stringType .=  "s";
  $withdrawUpdated = updateDynamicData($conn,"loan_status"," WHERE id = ? ",$tableName,$tableValue,$stringType);

  if($withdrawUpdated)
  {
    header('Location: ../invoiceRecordMulti.php');
  }
}
//========================================================================================================================================================

  if (isset($_POST['addCheckID'])) {

$checkID = rewrite($_POST['check_id']);
$receiveDate = rewrite($_POST['receive_date']);
$receiveStatus = 'COMPLETED';
$invoiceMultiId = rewrite($_POST['id']);

$multiInvoiceDetails = getMultiInvoice($conn, "WHERE id = ?", array("id"), array($_POST['id']), "s");

echo $unitDetails = $multiInvoiceDetails[0]->getUnitNo(); // value seperated by MongoCommandCursor
$unitDetailsExplode = explode(",",$unitDetails);

$claimStatusDetails = $multiInvoiceDetails[0]->getClaimsStatus(); // value seperated by MongoCommandCursor
$claimStatusExplode = explode(",",$claimStatusDetails);


if ($unitDetailsExplode) {

  for ($cnt=0; $cnt <count($unitDetailsExplode) ; $cnt++) {

    $loanDetails = getLoanStatus($conn, "WHERE unit_no = ?", array("unit_no"), array($unitDetailsExplode[$cnt]), "s");
        $purchaserName = $loanDetails[0]->getPurchaserName();
        $unitNo =  $loanDetails[0]->getUnitNo();
        $loanUid = $loanDetails[0]->getLoanUid();
        $bookingDate = $loanDetails[0]->getBookingDate();

        $id = $loanDetails[0]->getId();
        $unitDetailsExplode[$cnt];
        $claimStatusExplode[$cnt];

    $projectName = $loanDetails[0]->getProjectName();
    $projectDetails = getProject($conn, "WHERE project_name = ?", array("project_name"), array($projectName), "s");
    $projectClaims = $projectDetails[0]->getProjectClaims();
    $ulOverrideFinal = $loanDetails[0]->getUlOverride() / $projectClaims;
    $uulOverrideFinal = $loanDetails[0]->getUulOverride() / $projectClaims;
    $plOverrideFinal = $loanDetails[0]->getPlOverride() / $projectClaims;
    $hosOverrideFinal = $loanDetails[0]->getHosOverride() / $projectClaims;
    $listerOverrideFinal = $loanDetails[0]->getListerOverride() / $projectClaims;
    // $agentCommFinal = $loanDetails[0]->getAgentComm() - $agentAdvanced / $projectClaims;

    $agent = $loanDetails[0]->getAgent();
    $agentComm = $loanDetails[0]->getAgentComm();
    $agentAdvanced = 2000;
    $agentCommFinal = ($agentComm - $agentAdvanced) / $projectClaims;

    $upline1 = $loanDetails[0]->getUpline1();
    $ulOverride = $loanDetails[0]->getUlOverride();
    $ulOverrideFinal = $ulOverride / $projectClaims;

    $upline2 = $loanDetails[0]->getUpline2();
    $uulOverride = $loanDetails[0]->getUulOverride();
    $uulOverrideFinal = $uulOverride / $projectClaims;

    $plName = $loanDetails[0]->getPlName();
    $plOverride = $loanDetails[0]->getPlOverride();
    $plOverrideFinal = $plOverride / $projectClaims;

    $hosName = $loanDetails[0]->getHosName();
    $hosOverride = $loanDetails[0]->getHosOverride();
    $hosOverrideFinal = $hosOverride / $projectClaims;

    $listerName = $loanDetails[0]->getListerName();
    $listerOverride = $loanDetails[0]->getListerOverride();
    $listerOverrideFinal = $listerOverride / $projectClaims;

if ($claimStatusExplode[$cnt] == 1) {
  $details = "1st Commission";
  // $receiveDate1 = date('d-m-Y');

  $tableName = array();
  $tableValue =  array();
  $stringType =  "";
  // //echo "save to database";
  if($checkID)
  {
      array_push($tableName,"check_no1");
      array_push($tableValue,$checkID);
      $stringType .=  "s";
  }
  if ($checkID) {
    array_push($tableName,"receive_date1");
    array_push($tableValue,$receiveDate);
    $stringType .=  "s";
  }

  // if ($totalClaimedDevAmt && !$loanDetails[0]->getCheckNo1()) {
  //   array_push($tableName,"total_claimed_dev_amt");
  //   array_push($tableValue,$totalClaimedDevAmt);
  //   $stringType .=  "s";
  // }
  array_push($tableValue,$id);
  $stringType .=  "s";
  $withdrawUpdated = updateDynamicData($conn,"loan_status"," WHERE id = ? ",$tableName,$tableValue,$stringType);

  if($withdrawUpdated)
  {
    header('Location: ../invoiceRecordMulti.php');
  }

  $tableName = array();
  $tableValue =  array();
  $stringType =  "";

  if ($checkID) {
    array_push($tableName,"check_id");
    array_push($tableValue,$checkID);
    $stringType .=  "s";
  }
  if ($receiveDate) {
    array_push($tableName,"receive_date");
    array_push($tableValue,$receiveDate);
    $stringType .=  "s";
  }
  array_push($tableValue,$invoiceMultiId);
  $stringType .=  "s";
  $withdrawUpdated = updateDynamicData($conn,"invoice_multi"," WHERE id = ? ",$tableName,$tableValue,$stringType);

  if($withdrawUpdated)
  {
    header('Location: ../invoiceRecordMulti.php');
  }
  if (!$loanDetails[0]->getCheckNo1()) {

      if(addUplineCommission($conn, $idd, $purchaserName, $loanUid, $ulOverrideFinal,$upline1,$projectName,$unitNo,$bookingDate, $receiveStatus, $details))
           {
                //$_SESSION['messageType'] = 1;
                // header('Location: ../invoiceRecordMulti.php');
                //echo "register success";
           }

     if (addUpuplineCommission($conn, $idd2, $purchaserName, $loanUid, $uulOverrideFinal,$upline2,$projectName,$unitNo,$bookingDate, $receiveStatus, $details))
     {
       header('Location: ../invoiceRecordMulti.php');
     }
     if (addAgentCommission($conn, $idd3, $purchaserName, $loanUid, $agentCommFinal,$agent,$projectName,$unitNo,$bookingDate, $receiveStatus, $details))
     {
       header('Location: ../invoiceRecordMulti.php');
     }
     if ($plName && $plOverrideFinal) {
       if (addPlCommission($conn, $idd3, $purchaserName, $loanUid, $plOverrideFinal,$plName,$projectName,$unitNo,$bookingDate, $receiveStatus, $details))
       {
         header('Location: ../invoiceRecordMulti.php');
       }
     }
     if ($hosName && $hosOverrideFinal) {
       if (addHosCommission($conn, $idd3, $purchaserName, $loanUid, $hosOverrideFinal,$hosName,$projectName,$unitNo,$bookingDate, $receiveStatus, $details))
       {
         header('Location: ../invoiceRecordMulti.php');
       }
     }
     if ($listerName && $listerOverrideFinal) {
       if (addListerCommission($conn, $idd3, $purchaserName, $loanUid, $listerOverrideFinal,$listerName,$projectName,$unitNo,$bookingDate, $receiveStatus, $details))
       {
         header('Location: ../invoiceRecordMulti.php');
       }
     }


  }
}
if ($claimStatusExplode[$cnt] == 2) {
  $details = "2nd Commission";
  // $receiveDate1 = date('d-m-Y');

  $tableName = array();
  $tableValue =  array();
  $stringType =  "";
  // //echo "save to database";
  if($checkID)
  {
      array_push($tableName,"check_no2");
      array_push($tableValue,$checkID);
      $stringType .=  "s";
  }
  if ($checkID) {
    array_push($tableName,"receive_date2");
    array_push($tableValue,$receiveDate);
    $stringType .=  "s";
  }
  array_push($tableValue,$id);
  $stringType .=  "s";
  $withdrawUpdated = updateDynamicData($conn,"loan_status"," WHERE id = ? ",$tableName,$tableValue,$stringType);

  if($withdrawUpdated)
  {
    header('Location: ../invoiceRecordMulti.php');
  }
  if ($checkID) {
    array_push($tableName,"check_id");
    array_push($tableValue,$checkID);
    $stringType .=  "s";
  }
  array_push($tableValue,$invoiceMultiId);
  $stringType .=  "s";
  $withdrawUpdated = updateDynamicData($conn,"invoice_multi"," WHERE id = ? ",$tableName,$tableValue,$stringType);

  if($withdrawUpdated)
  {
    header('Location: ../invoiceRecordMulti.php');
  }
  $tableName = array();
  $tableValue =  array();
  $stringType =  "";

  if ($checkID) {
    array_push($tableName,"check_id");
    array_push($tableValue,$checkID);
    $stringType .=  "s";
  }
  if ($receiveDate) {
    array_push($tableName,"receive_date");
    array_push($tableValue,$receiveDate);
    $stringType .=  "s";
  }
  array_push($tableValue,$invoiceMultiId);
  $stringType .=  "s";
  $withdrawUpdated = updateDynamicData($conn,"invoice_multi"," WHERE id = ? ",$tableName,$tableValue,$stringType);

  if($withdrawUpdated)
  {
    header('Location: ../invoiceRecordMulti.php');
  }
  if (!$loanDetails[0]->getCheckNo2()) {

      if(addUplineCommission($conn, $idd, $purchaserName, $loanUid, $ulOverrideFinal,$upline1,$projectName,$unitNo,$bookingDate, $receiveStatus, $details))
           {
                //$_SESSION['messageType'] = 1;
                // header('Location: ../invoiceRecordMulti.php');
                //echo "register success";
           }

     if (addUpuplineCommission($conn, $idd2, $purchaserName, $loanUid, $uulOverrideFinal,$upline2,$projectName,$unitNo,$bookingDate, $receiveStatus, $details))
     {
       header('Location: ../invoiceRecordMulti.php');
     }
     if (addAgentCommission($conn, $idd3, $purchaserName, $loanUid, $agentCommFinal,$agent,$projectName,$unitNo,$bookingDate, $receiveStatus, $details))
     {
       header('Location: ../invoiceRecordMulti.php');
     }
     if ($plName && $plOverrideFinal) {
       if (addPlCommission($conn, $idd3, $purchaserName, $loanUid, $plOverrideFinal,$plName,$projectName,$unitNo,$bookingDate, $receiveStatus, $details))
       {
         header('Location: ../invoiceRecordMulti.php');
       }
     }
     if ($hosName && $hosOverrideFinal) {
       if (addHosCommission($conn, $idd3, $purchaserName, $loanUid, $hosOverrideFinal,$hosName,$projectName,$unitNo,$bookingDate, $receiveStatus, $details))
       {
         header('Location: ../invoiceRecordMulti.php');
       }
     }
     if ($listerName && $listerOverrideFinal) {
       if (addListerCommission($conn, $idd3, $purchaserName, $loanUid, $listerOverrideFinal,$listerName,$projectName,$unitNo,$bookingDate, $receiveStatus, $details))
       {
         header('Location: ../invoiceRecordMulti.php');
       }
     }


  }
}
if ($claimStatusExplode[$cnt] == 3) {
  $details = "3rd Commission";
  // $receiveDate1 = date('d-m-Y');

  $tableName = array();
  $tableValue =  array();
  $stringType =  "";
  // //echo "save to database";
  if($checkID)
  {
      array_push($tableName,"check_no3");
      array_push($tableValue,$checkID);
      $stringType .=  "s";
  }
  if ($checkID) {
    array_push($tableName,"receive_date3");
    array_push($tableValue,$receiveDate);
    $stringType .=  "s";
  }
  array_push($tableValue,$id);
  $stringType .=  "s";
  $withdrawUpdated = updateDynamicData($conn,"loan_status"," WHERE id = ? ",$tableName,$tableValue,$stringType);

  if($withdrawUpdated)
  {
    header('Location: ../invoiceRecordMulti.php');
  }
  if ($checkID) {
    array_push($tableName,"check_id");
    array_push($tableValue,$checkID);
    $stringType .=  "s";
  }
  array_push($tableValue,$invoiceMultiId);
  $stringType .=  "s";
  $withdrawUpdated = updateDynamicData($conn,"invoice_multi"," WHERE id = ? ",$tableName,$tableValue,$stringType);

  if($withdrawUpdated)
  {
    header('Location: ../invoiceRecordMulti.php');
  }
  $tableName = array();
  $tableValue =  array();
  $stringType =  "";

  if ($checkID) {
    array_push($tableName,"check_id");
    array_push($tableValue,$checkID);
    $stringType .=  "s";
  }
  if ($receiveDate) {
    array_push($tableName,"receive_date");
    array_push($tableValue,$receiveDate);
    $stringType .=  "s";
  }
  array_push($tableValue,$invoiceMultiId);
  $stringType .=  "s";
  $withdrawUpdated = updateDynamicData($conn,"invoice_multi"," WHERE id = ? ",$tableName,$tableValue,$stringType);

  if($withdrawUpdated)
  {
    header('Location: ../invoiceRecordMulti.php');
  }
  if (!$loanDetails[0]->getCheckNo3()) {

      if(addUplineCommission($conn, $idd, $purchaserName, $loanUid, $ulOverrideFinal,$upline1,$projectName,$unitNo,$bookingDate, $receiveStatus, $details))
           {
                //$_SESSION['messageType'] = 1;
                // header('Location: ../invoiceRecordMulti.php');
                //echo "register success";
           }

     if (addUpuplineCommission($conn, $idd2, $purchaserName, $loanUid, $uulOverrideFinal,$upline2,$projectName,$unitNo,$bookingDate, $receiveStatus, $details))
     {
       header('Location: ../invoiceRecordMulti.php');
     }
     if (addAgentCommission($conn, $idd3, $purchaserName, $loanUid, $agentCommFinal,$agent,$projectName,$unitNo,$bookingDate, $receiveStatus, $details))
     {
       header('Location: ../invoiceRecordMulti.php');
     }
     if ($plName && $plOverrideFinal) {
       if (addPlCommission($conn, $idd3, $purchaserName, $loanUid, $plOverrideFinal,$plName,$projectName,$unitNo,$bookingDate, $receiveStatus, $details))
       {
         header('Location: ../invoiceRecordMulti.php');
       }
     }
     if ($hosName && $hosOverrideFinal) {
       if (addHosCommission($conn, $idd3, $purchaserName, $loanUid, $hosOverrideFinal,$hosName,$projectName,$unitNo,$bookingDate, $receiveStatus, $details))
       {
         header('Location: ../invoiceRecordMulti.php');
       }
     }
     if ($listerName && $listerOverrideFinal) {
       if (addListerCommission($conn, $idd3, $purchaserName, $loanUid, $listerOverrideFinal,$listerName,$projectName,$unitNo,$bookingDate, $receiveStatus, $details))
       {
         header('Location: ../invoiceRecordMulti.php');
       }
     }
  }
}
if ($claimStatusExplode[$cnt] == 4) {
  $details = "4th Commission";
  // $receiveDate = date('d-m-Y');

  $tableName = array();
  $tableValue =  array();
  $stringType =  "";
  // //echo "save to database";
  if($checkID)
  {
      array_push($tableName,"check_no4");
      array_push($tableValue,$checkID);
      $stringType .=  "s";
  }
  if ($checkID) {
    array_push($tableName,"receive_date4");
    array_push($tableValue,$receiveDate);
    $stringType .=  "s";
  }
  array_push($tableValue,$id);
  $stringType .=  "s";
  $withdrawUpdated = updateDynamicData($conn,"loan_status"," WHERE id = ? ",$tableName,$tableValue,$stringType);

  if($withdrawUpdated)
  {
    header('Location: ../invoiceRecordMulti.php');
  }
  if ($checkID) {
    array_push($tableName,"check_id");
    array_push($tableValue,$checkID);
    $stringType .=  "s";
  }
  array_push($tableValue,$invoiceMultiId);
  $stringType .=  "s";
  $withdrawUpdated = updateDynamicData($conn,"invoice_multi"," WHERE id = ? ",$tableName,$tableValue,$stringType);

  if($withdrawUpdated)
  {
    header('Location: ../invoiceRecordMulti.php');
  }
  $tableName = array();
  $tableValue =  array();
  $stringType =  "";

  if ($checkID) {
    array_push($tableName,"check_id");
    array_push($tableValue,$checkID);
    $stringType .=  "s";
  }
  if ($receiveDate) {
    array_push($tableName,"receive_date");
    array_push($tableValue,$receiveDate);
    $stringType .=  "s";
  }
  array_push($tableValue,$invoiceMultiId);
  $stringType .=  "s";
  $withdrawUpdated = updateDynamicData($conn,"invoice_multi"," WHERE id = ? ",$tableName,$tableValue,$stringType);

  if($withdrawUpdated)
  {
    header('Location: ../invoiceRecordMulti.php');
  }
  if (!$loanDetails[0]->getCheckNo4()) {

      if(addUplineCommission($conn, $idd, $purchaserName, $loanUid, $ulOverrideFinal,$upline1,$projectName,$unitNo,$bookingDate, $receiveStatus, $details))
           {
                //$_SESSION['messageType'] = 1;
                // header('Location: ../invoiceRecordMulti.php');
                //echo "register success";
           }

     if (addUpuplineCommission($conn, $idd2, $purchaserName, $loanUid, $uulOverrideFinal,$upline2,$projectName,$unitNo,$bookingDate, $receiveStatus, $details))
     {
       header('Location: ../invoiceRecordMulti.php');
     }
     if (addAgentCommission($conn, $idd3, $purchaserName, $loanUid, $agentCommFinal,$agent,$projectName,$unitNo,$bookingDate, $receiveStatus, $details))
     {
       header('Location: ../invoiceRecordMulti.php');
     }
     if ($plName && $plOverrideFinal) {
       if (addPlCommission($conn, $idd3, $purchaserName, $loanUid, $plOverrideFinal,$plName,$projectName,$unitNo,$bookingDate, $receiveStatus, $details))
       {
         header('Location: ../invoiceRecordMulti.php');
       }
     }
     if ($hosName && $hosOverrideFinal) {
       if (addHosCommission($conn, $idd3, $purchaserName, $loanUid, $hosOverrideFinal,$hosName,$projectName,$unitNo,$bookingDate, $receiveStatus, $details))
       {
         header('Location: ../invoiceRecordMulti.php');
       }
     }
     if ($listerName && $listerOverrideFinal) {
       if (addListerCommission($conn, $idd3, $purchaserName, $loanUid, $listerOverrideFinal,$listerName,$projectName,$unitNo,$bookingDate, $receiveStatus, $details))
       {
         header('Location: ../invoiceRecordMulti.php');
       }
     }


  }
}
if ($claimStatusExplode[$cnt] == 5) {
  $details = "5th Commission";
  // $receiveDate1 = date('d-m-Y');

  $tableName = array();
  $tableValue =  array();
  $stringType =  "";
  // //echo "save to database";
  if($checkID)
  {
      array_push($tableName,"check_no5");
      array_push($tableValue,$checkID);
      $stringType .=  "s";
  }
  if ($checkID) {
    array_push($tableName,"receive_date5");
    array_push($tableValue,$receiveDate);
    $stringType .=  "s";
  }
  array_push($tableValue,$id);
  $stringType .=  "s";
  $withdrawUpdated = updateDynamicData($conn,"loan_status"," WHERE id = ? ",$tableName,$tableValue,$stringType);

  if($withdrawUpdated)
  {
    header('Location: ../invoiceRecordMulti.php');
  }
  if ($checkID) {
    array_push($tableName,"check_id");
    array_push($tableValue,$checkID);
    $stringType .=  "s";
  }
  array_push($tableValue,$invoiceMultiId);
  $stringType .=  "s";
  $withdrawUpdated = updateDynamicData($conn,"invoice_multi"," WHERE id = ? ",$tableName,$tableValue,$stringType);

  if($withdrawUpdated)
  {
    header('Location: ../invoiceRecordMulti.php');
  }
  $tableName = array();
  $tableValue =  array();
  $stringType =  "";

  if ($checkID) {
    array_push($tableName,"check_id");
    array_push($tableValue,$checkID);
    $stringType .=  "s";
  }
  if ($receiveDate) {
    array_push($tableName,"receive_date");
    array_push($tableValue,$receiveDate);
    $stringType .=  "s";
  }
  array_push($tableValue,$invoiceMultiId);
  $stringType .=  "s";
  $withdrawUpdated = updateDynamicData($conn,"invoice_multi"," WHERE id = ? ",$tableName,$tableValue,$stringType);

  if($withdrawUpdated)
  {
    header('Location: ../invoiceRecordMulti.php');
  }
  if (!$loanDetails[0]->getCheckNo5()) {

      if(addUplineCommission($conn, $idd, $purchaserName, $loanUid, $ulOverrideFinal,$upline1,$projectName,$unitNo,$bookingDate, $receiveStatus, $details))
           {
                //$_SESSION['messageType'] = 1;
                // header('Location: ../invoiceRecordMulti.php');
                //echo "register success";
           }

     if (addUpuplineCommission($conn, $idd2, $purchaserName, $loanUid, $uulOverrideFinal,$upline2,$projectName,$unitNo,$bookingDate, $receiveStatus, $details))
     {
       header('Location: ../invoiceRecordMulti.php');
     }
     if (addAgentCommission($conn, $idd3, $purchaserName, $loanUid, $agentCommFinal,$agent,$projectName,$unitNo,$bookingDate, $receiveStatus, $details))
     {
       header('Location: ../invoiceRecordMulti.php');
     }
     if ($plName && $plOverrideFinal) {
       if (addPlCommission($conn, $idd3, $purchaserName, $loanUid, $plOverrideFinal,$plName,$projectName,$unitNo,$bookingDate, $receiveStatus, $details))
       {
         header('Location: ../invoiceRecordMulti.php');
       }
     }
     if ($hosName && $hosOverrideFinal) {
       if (addHosCommission($conn, $idd3, $purchaserName, $loanUid, $hosOverrideFinal,$hosName,$projectName,$unitNo,$bookingDate, $receiveStatus, $details))
       {
         header('Location: ../invoiceRecordMulti.php');
       }
     }
     if ($listerName && $listerOverrideFinal) {
       if (addListerCommission($conn, $idd3, $purchaserName, $loanUid, $listerOverrideFinal,$listerName,$projectName,$unitNo,$bookingDate, $receiveStatus, $details))
       {
         header('Location: ../invoiceRecordMulti.php');
       }
     }


  }
}







  }
}
}

?>
