<?php
if (session_id() == ""){
    session_start();
}

require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/User.php';
require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';
require_once dirname(__FILE__) . '/utilities/languageFunction.php';


function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

?>

<!doctype html>
<html>
<head>
<?php include 'meta.php'; ?>
<!--<meta property="og:url" content="https://dcksupreme.asia/" />-->
<meta property="og:title" content="GIC" />
<title>GIC</title>
<!--<link rel="canonical" href="https://dcksupreme.asia/" />-->
<?php include 'css.php'; ?>
<?php require_once dirname(__FILE__) . '/header.php'; ?>
</head>

<body class="body">



    <div class="headerline"></div>
    <div class="background screen_padding">
        <div class="logo"><img src="img/gic.png" class="logo-size"></div>
        <div class="clear"></div>

        <div class="container float-left">

          <div class="title">
            <label><b><h2><u class="underline">Wel</u>come to GIC Group </h2></b></label>
          </div>
          <div class="clear"></div>
    <?php
      $oilxag_uname = null;
      $oilxag_password = null;



    ?>
          <form method="POST" action="utilities/loginFunction.php">
            <label class="labelSize">Username:</label>
              <input style="outline: none" class="inputSize input-pattern" type="text"  placeholder="Username" name="username"><br>

            <label class="labelSize">Password:</label>
              <input style="outline: none" class="inputSize input-pattern" type="password" placeholder="" name="password" id="login_password" ><br>

            <button class="button" type="submit" name="loginButton">Submit</button><br>
          </form>

          <!-- <div class="forgotPwd_link">
            <p><a class="forgotPwd_linkDetails" href="#">Forgot Password ?</a></p>
          </div>-->

        </div>
        <div class="clear"></div>
     </div>
<?php

if( $_SERVER['REQUEST_METHOD'] == 'POST' && isset($_POST['send_email_button'])) {

    // EDIT THE 2 LINES BELOW AS REQUIRED
    $email_to = "sherry2.vidatech@gmail.com";
    $email_subject = "Contact Form via DCK Supreme website";

    function died($error)
	{
        // your error code can go here
		echo '<script>alert("We are very sorry, but there were error(s) found with the form you submitted.\n\nThese errors appear below.\n\n';
		echo $error;
        echo '\n\nPlease go back and fix these errors.\n\n")</script>';
        die();
    }


    // validation expected data exists
    if(!isset($_POST['name']) ||
        !isset($_POST['email']) ||
		!isset($_POST['telephone'])) {
        died('We are sorry, but there appears to be a problem with the form you submitted.');
    }



    $first_name = $_POST['name']; // required
    $email_from = $_POST['email']; // required
	$telephone = $_POST['telephone']; //required
    $comments = $_POST['comments'];
    $contactOption = $_POST['contact-option']; // required
    $contactMethod = null;

	//$error_message = '<script>alert("The name you entered does not appear to be valid.");</script>';
	//if($first_name == ""){
	//	echo $error_message;
	//}

    if($contactOption == null || $contactOption == ""){
        $contactMethod = "don\'t bother me";
    }else if($contactOption == "contact-more-info"){
        $contactMethod = "I want to be contacted with more information about your company's offering marketing services and consulting";
    }else if($contactOption == "contact-on-request"){
        $contactMethod = "I just want to be contacted based on my request/ inquiry";
    }else{
        $contactMethod = "error getting contact options";
		$error_message .="Error getting contact options\n\n";
    }

    $error_message = "";
    $email_exp = '/^[A-Za-z0-9._%-]+@[A-Za-z0-9.-]+\.[A-Za-z]{2,4}$/';

  if(!preg_match($email_exp,$email_from)) {
    $error_message .= 'The email address you entered does not appear to be valid.\n';
  }


    $string_exp = "/^[A-Za-z .'-]+$/";

  if(!preg_match($string_exp,$first_name)) {
    $error_message .= 'The name you entered does not appear to be valid.\n';
  }




  if(strlen($error_message) > 0) {
    died($error_message);
  }

    $email_message = "Form details below.\n\n";


    function clean_string($string) {
      $bad = array("content-type","bcc:","to:","cc:","href");
      return str_replace($bad,"",$string);
    }

    $email_message .= "Name: ".clean_string($first_name)."\n";
    $email_message .= "Email: ".clean_string($email_from)."\n";
	$email_message .= "Telephone: ".clean_string($telephone)."\n";
    $email_message .= "Message : ".clean_string($comments)."\n";
    $email_message .= "Contact Option : ".clean_string($contactMethod)."\n";

// create email headers
$headers = 'From: '.$email_from."\r\n".
'Reply-To: '.$email_from."\r\n" .
'X-Mailer: PHP/' . phpversion();
@mail($email_to, $email_subject, $email_message, $headers);
echo '<script>alert("Thank you! We will be in contact shortly!")</script>';

?>
<!-- include your own success html here -->

<!--Thank you for contacting us. We will be in touch with you very soon.-->
<?php

}
?>
<?php include 'js.php'; ?>
<?php
if(isset($_GET['type']))
{
    $messageType = null;

    if($_SESSION['messageType'] == 1)
    {
        if($_GET['type'] == 1)
        {
            // $messageType = "Successfully register your profile! Please confirm your registration inside of your email.";
            $messageType = "Register Successfully!";
        }
        else if($_GET['type'] == 2)
        {
            $messageType = "There are no referrer with this email ! Please register again";
        }
        else if($_GET['type'] == 3)
        {
            // $messageType = "Successfully register your profile! Please confirm your registration inside of your email.";
            $messageType = "Register Successfully!";
        }
        else if($_GET['type'] == 4)
        {
            $messageType = "User password must be more than 5 !";
        }
        else if($_GET['type'] == 5)
        {
            $messageType = "User password does not match";
        }
        else if($_GET['type'] == 6)
        {
            $messageType = "Wrong email format.";
        }
        else if($_GET['type'] == 7)
        {
            $messageType = "There are no user with this email ! Please try again.";
        }
        else if($_GET['type'] == 8)
        {
            $messageType = "Successfully reset your password! Please check your email.";
        }
        else if($_GET['type'] == 9)
        {
            $messageType = "Successfully reset your password! ";
        }
        else if($_GET['type'] == 10)
        {
            $messageType = "Please confirm your registration inside your email! ";
        }
        else if($_GET['type'] == 11)
        {
            $messageType = "Incorrect email or password! ";
        }
        echo '
        <script>
            putNoticeJavascript("Notice !! ","'.$messageType.'");
        </script>
        ';
        $_SESSION['messageType'] = 0;
    }
}
if(isset($_GET['promptError']))
{
    $messageType = null;

    if($_GET['promptError'] == 1)
    {
        $messageType = "Error registering new account.The account already exist";
    }
    else if($_GET['promptError'] == 2)
    {
        $messageType = "Error assigning referral relationship. Please register again.";
    }
    echo '
    <script>
        putNoticeJavascript("Notice !! ","'.$messageType.'");
    </script>
    ';
}
?>
<script src="js/jssor.slider.min.js" type="text/javascript"></script>
<script type="text/javascript">
        jssor_1_slider_init = function() {

            var jssor_1_options = {
              $AutoPlay: 1,
              $SlideDuration: 800,
              $SlideEasing: $Jease$.$OutQuint,
              $ArrowNavigatorOptions: {
                $Class: $JssorArrowNavigator$
              },
              $BulletNavigatorOptions: {
                $Class: $JssorBulletNavigator$
              }
            };

            var jssor_1_slider = new $JssorSlider$("jssor_1", jssor_1_options);

            /*#region responsive code begin*/

            var MAX_WIDTH = 3000;

            function ScaleSlider() {
                var containerElement = jssor_1_slider.$Elmt.parentNode;
                var containerWidth = containerElement.clientWidth;

                if (containerWidth) {

                    var expectedWidth = Math.min(MAX_WIDTH || containerWidth, containerWidth);

                    jssor_1_slider.$ScaleWidth(expectedWidth);
                }
                else {
                    window.setTimeout(ScaleSlider, 30);
                }
            }

            ScaleSlider();

            $Jssor$.$AddEvent(window, "load", ScaleSlider);
            $Jssor$.$AddEvent(window, "resize", ScaleSlider);
            $Jssor$.$AddEvent(window, "orientationchange", ScaleSlider);
            /*#endregion responsive code end*/
        };
    </script>


    <script type="text/javascript">jssor_1_slider_init();</script>
    <!-- #endregion Jssor Slider End -->

</body>
</html>
