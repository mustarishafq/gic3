<?php
class AdvancedSlip {
    /* Member variables */
    var $id,$unitNo, $projectName, $bookingDate, $loanUid, $agent, $amount, $status, $checkID, $receiveStatus, $dateCreated, $dateUpdated;

    /**
     * @return mixed
     */
    public function getID()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setID($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getUnitNo()
    {
        return $this->unit_no;
    }

    /**
     * @param mixed $id
     */
    public function setUnitNo($unitNo)
    {
        $this->unit_no = $unitNo;
    }

    /**
     * @return mixed
     */
    public function getCheckID()
    {
        return $this->check_id;
    }

    /**
     * @param mixed $id
     */
    public function setCheckID($checkID)
    {
        $this->check_id = $checkID;
    }

    /**
     * @return mixed
     */
    public function getReceiveStatus()
    {
        return $this->receive_status;
    }

    /**
     * @param mixed $id
     */
    public function setReceiveStatus($receiveStatus)
    {
        $this->receive_status = $receiveStatus;
    }

    /**
     * @return mixed
     */
    public function getProjectName()
    {
        return $this->project_name;
    }

    /**
     * @param mixed $id
     */
    public function setProjectName($projectName)
    {
        $this->project_name = $projectName;
    }

    /**
     * @return mixed
     */
    public function getBookingDate()
    {
        return $this->bookingDate;
    }

    /**
     * @param mixed $bookingDate
     */
    public function setBookingDate($bookingDate)
    {
        $this->bookingDate = $bookingDate;
    }

    /**
     * @return mixed
     */
    public function getAgent()
    {
        return $this->agent;
    }

    /**
     * @param mixed $id
     */
    public function setAgent($agent)
    {
        $this->agent = $agent;
    }

    /**
     * @return mixed
     */
    public function getAmount()
    {
        return $this->amount;
    }

    /**
     * @param mixed $id
     */
    public function setAmount($amount)
    {
        $this->amount = $amount;
    }

    /**
     * @return mixed
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param mixed $id
     */
    public function setStatus($status)
    {
        $this->status = $status;
    }

    /**
     * @return mixed
     */
    public function getDateCreated()
    {
        return $this->dateCreated;
    }

    /**
     * @param mixed $dateCreated
     */
    public function setDateCreated($dateCreated)
    {
        $this->dateCreated = $dateCreated;
    }

    /**
     * @return mixed
     */
    public function getLoanUid()
    {
        return $this->loan_uid;
    }

    /**
     * @param mixed $dateUpdated
     */
    public function setLoanUid($loanUid)
    {
        $this->loan_uid = $loanUid;
    }

    /**
     * @return mixed
     */
    public function getDateUpdated()
    {
        return $this->date_updated;
    }

    /**
     * @param mixed $dateUpdated
     */
    public function setDateUpdated($dateUpdated)
    {
        $this->date_updated = $dateUpdated;
    }

}

function getAdvancedSlip($conn,$whereClause = null,$queryColumns = null,$queryValues = null,$queryTypes = null){
    $dbColumnNames = array("id", "unit_no", "project_name", "booking_date", "loan_uid", "agent", "amount", "status", "check_id", "receive_status", "date_created","date_updated");

    $sql = sqlSelectSimpleBuilder($dbColumnNames,"advance_slip");
    if($whereClause){
        $sql .= $whereClause;
    }

    if($stmt = $conn->prepare($sql)){
        /*
             Binds variables to prepared statement

             i    corresponding variable has type integer
             d    corresponding variable has type double
             s    corresponding variable has type string
             b    corresponding variable is a blob and will be sent in packets
        */

        if($queryColumns&&$queryTypes&&$queryValues){
            $stmt = returnStmtWithDynamicBinding($stmt,$queryValues,$queryTypes);
        }

//        $stmt->bind_param('s',$queryValues[0]);

        /* execute query */
        $stmt->execute();

        /* Store the result (to get properties) */
        $stmt->store_result();

        /* Get the number of rows */
        $num_of_rows = $stmt->num_rows;

        /* Bind the result to variables */
        $stmt->bind_result($id, $unitNo, $projectName, $bookingDate, $loanUid, $agent, $amount, $status, $checkID, $receiveStatus, $dateCreated, $dateUpdated);

        $resultRows = array();
        while ($stmt->fetch()) {
            $class = new AdvancedSlip();
            $class->setID($id);
            $class->setUnitNo($unitNo);
            $class->setProjectName($projectName);

            $class->setBookingDate($bookingDate);

            $class->setLoanUid($loanUid);
            $class->setAgent($agent);
            $class->setAmount($amount);
            $class->setStatus($status);
            $class->setCheckID($checkID);
            $class->setReceiveStatus($receiveStatus);
            $class->setDateCreated($dateCreated);
            $class->setDateUpdated($dateUpdated);

            array_push($resultRows,$class);
        }

        /* free results */
        $stmt->free_result();

        /* close statement */
        $stmt->close();

        if($num_of_rows <= 0){
            return null;
        }else{
            return $resultRows;
        }
    }else{
//        echo "Prepare Error: ($conn->errno) $conn->error";
        return null;
    }

}
