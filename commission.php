<?php

require_once dirname(__FILE__) . '/1dbCon/dbCon.php';
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/classes/Invoice.php';
require_once dirname(__FILE__) . '/classes/Product2.php';
require_once dirname(__FILE__) . '/classes/AdvancedSlip.php';
require_once dirname(__FILE__) . '/classes/Commission.php';
require_once dirname(__FILE__) . '/classes/User.php';

require_once dirname(__FILE__) . '/utilities/allNoticeModals.php';
require_once dirname(__FILE__) . '/utilities/convertingRinggit.php';
require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';
require_once dirname(__FILE__) . '/utilities/languageFunction.php';

$conn = connDB();

$commissionDetails = getCommission($conn, "WHERE id =?",array("id"),array($_POST['id']), "s");

$userDetails = getUser($conn, "WHERE username = ? ", array("username"), array($commissionDetails[0]->getUpline()), "s");
// $commissionDetails = getInvoice($conn, "WHERE id =? ORDER BY id DESC LIMIT 1",array("id"),array($_POST['invoice']), "s");
$id = $_POST['id'];

// $projectName = $_POST['project_name'];
$status = 'COMPLETED';

// $conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <?php require_once dirname(__FILE__) . '/header.php'; ?>
	<?php include 'meta.php'; ?>
    <!--<meta property="og:url" content="https://dcksupreme.asia/" />-->
    <meta property="og:title" content="Commission | GIC" />
    <title>Commission | GIC</title>
    <!--<link rel="canonical" href="https://dcksupreme.asia/" />-->
    <?php include 'css.php'; ?>
</head>
<body class="body">

<?php  include 'admin2Header.php'; ?>

<?php echo '<script type="text/javascript" src="js/jquery-3.3.1.min.js"></script>'; ?>

<div class="yellow-body same-padding">
	<h1 class="h1-title h1-before-border shipping-h1"   onclick="goBack()">
    	<a  class="black-white-link2 hover1">
    	    <img src="img/back.png" class="back-btn2 hover1a" alt="back" title="back">
            <img src="img/back3.png" class="back-btn2 hover1b" alt="back" title="back">
        	Commission
        </a>
    </h1>
    <div class="spacing-left short-red-border"></div>
    <!-- This is a filter for the table result -->


    <!-- <select class="filter-select clean">
    	<option class="filter-option">Latest Shipping</option>
        <option class="filter-option">Oldest Shipping</option>
    </select> -->

    <!-- End of Filter -->
    <div class="clear"></div>

    <div class="width100 print-div">
 		<div class="text-center">
            <img src="img/gic-logo.png" class="invoice-logo" alt="GIC Consultancy Sdn. Bhd." title="GIC Consultancy Sdn. Bhd.">
            <p class="invoice-address company-name"><b>GIC Consultancy Sdn. Bhd.</b></p>
            <p class="invoice-small-p">1189044-D (SST No : P11-1808-31038566)</p>
            <p class="invoice-address">1-2-42, Elit Avenue, Jalan Mayang Pasir 3, Bayan Lepas 11950 Penang.</p>
            <p class="invoice-address">Email: gicpenang@gmail.com  Tel:04-6374698</p>
        </div>
		<h1 class="invoice-title">COMMISSION / ALLOWANCE</h1>
       <div class="invoice-width50 top-invoice-w50">
			<table class="invoice-top-small-table left-table">
            	<tr>
                	<td>Date</td>
                    <td>:</td>
                    <td><?php echo $commissionDetails[0]->getDateUpdated(); ?></td>
                </tr>
                <tr>
                	<td>Pay to</td>
                    <td>:</td>
                    <td><?php echo $commissionDetails[0]->getUpline() ?></td>
                </tr>
            </table>
        </div>
        <div class="invoice-width50 top-invoice-w50">
			<table class="invoice-top-small-table">
            	<tr>
                	<td>Payslip No</td>
                    <td>:</td>
                    <td></td>
                </tr>
                <tr>
                	<td>NRIC</td>
                    <td>:</td>
                    <td><?php
            if ($userDetails) {
              echo $userDetails[0]->getIcNo();
            }else {
              echo "985624-20-6895";
            }
             ?></td>
                </tr>
            </table>
        </div>





        <div class="clear"></div>
        <table class="invoice-printing-table">
        	<thead>
                    <tr>
                    	<!-- <th >No.</th>
                        <th >Items</th>
                        <th >Amount (RM)</th> -->
                        <th>Project</th>
                        <th>Details</th>
                        <th>Unit</th>
                        <th>Nett Price (RM)</th>
                        <th>Amount (RM)</th>
                    </tr>
            </thead>



                      <tr>
                      	<td class="td"><?php echo $commissionDetails[0]->getProjectName()  ?></td>
                      <td class="td"><?php echo $commissionDetails[0]->getDetails()  ?></td>
                          <td class="td"><?php echo $commissionDetails[0]->getUnitNo()  ?></td>
                        <td class="td"><?php echo $commissionDetails[0]->getNettPrice()  ?></td>
                        <td class="td"><?php echo number_format($commissionDetails[0]->getCommission(), 2)  ?></td>
                    </tr>
                    <tr>
                      <td class="td"></td>
                        <td class="td"></td>
                      <td class="td"></td>
                      <td class="td"></td>
                  </tr>
                  <tr>
                    <td class="td"></td>
                      <td class="td"></td>
                    <td class="td"></td>
                    <td class="td"></td>
                </tr>
                <tr>
                  <td class="td"></td>
                    <td class="td"></td>
                  <td class="td"></td>
                  <td class="td"></td>
              </tr>
              <tr>
                <td class="td"></td>
                  <td class="td"></td>
                <td class="td"><b>Total :</b></td>
                <td class="td"><b><?php echo number_format($commissionDetails[0]->getCommission(), 2)  ?></b></td>
            </tr>
        </table>
		<div class="clear"></div>


        <div class="width100">
			<table  class="vtop-data" >
            	<tr>
                	<td></td>
                    <td></td>
                    <td></td>
                </tr>
                <tr>
                	<td>Total in Ringggit Malaysia</td>
                    <td><b>:</b></td>
                    <td><?php echo numtowords($commissionDetails[0]->getCommission())." Only" ?></td>
                </tr>
                <tr>
                	<td>Online Transfer</td>
                    <td><b>:</b></td>
                    <td>4645646</td>
                </tr>

            </table>
        </div>
        <div class="clear"></div>
        <div class="invoice-print-spacing"></div>
        <div style="margin-bottom: 0 auto" class="signature-div left-sign">
        	<div class="signature-border"></div>
            <p class="invoice-p">Approved by:</p>
            <p class="invoice-p"><b>Eddie Song</b></p>
        </div>
        <div style="margin-bottom: 0 auto" class="signature-div right-sign">
        	<div class="signature-border"></div>
            <p class="invoice-p">Received by:</p>
            <p class="invoice-p"><b>Name</b></p>
        </div>

	<div class="clear"></div>
    <?php if ($_SESSION['usertype_level'] == 3) {
      ?><div class="dual-button-div width100">
      	<a href="#">
              <button style="margin-left: 360px" class="mid-button red-btn clean"  onclick="window.print()">
                  Print
              </button>
          </a>
      </div><?php
    }else {
      ?><div class="dual-button-div width100">
      	<a href="#">
              <div class="left-button1  white-red-line-btn">
                  Edit
              </div>
          </a>
      	<a href="#">
              <button class="mid-button red-btn clean"  onclick="window.print()">
                  Print
              </button>
          </a>
          <form class="" action="utilities/sendToAgent.php" method="post">
            <input type="hidden" name="id" value="<?php echo $id ?>">
            <button type="submit" name="sendToAgent" class="clean white-red-line-btn right-button2" >
                Send Commission
            </button>
          </form>
      </div><?php
    } ?>


</div></div>




<?php require_once dirname(__FILE__) . '/footer.php'; ?>
<?php include 'jsAdmin.php'; ?>

<?php
if(isset($_GET['type']))
{
    $messageType = null;

    if($_SESSION['messageType'] == 1)
    {
        if($_GET['type'] == 1)
        {
            $messageType = "Server currently fail. Please try again later.";
        }
        else if($_GET['type'] == 2)
        {
            $messageType = "Successfully Delete Product.";
        }
        else if($_GET['type'] == 3)
        {
            $messageType = "Error Deleting Product";
        }
        echo '
        <script>
            putNoticeJavascript("Notice !! ","'.$messageType.'");
        </script>
        ';
        $_SESSION['messageType'] = 0;
    }
}
?>
<script>
$(function () {
    $('.link-to-details').click(function () {
        window.location.href = $(this).data('url');
    });
})

</script>
<script>
function goBack() {
  window.history.back();
}
</script>
</body>
</html>
